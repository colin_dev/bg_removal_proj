#include "stdafx.h"
#include "ImgFilger.h"


UINT32 g_dim;
cv::Mat g_inputImg;
cv::Mat g_outputImg;
cv::Mat g_imgChannel[4];
cv::Mat g_mask;
PBYTE g_pOldBuf;

void InitImgFilter(UINT32 dim, UINT32 bpp)
{
    g_dim = dim;
    g_pOldBuf = (PBYTE)malloc(g_dim * g_dim * 4);
    g_inputImg = cv::Mat(g_dim, g_dim, CV_8UC4, g_pOldBuf);
}

void DestroyImgFilter()
{
    if (g_pOldBuf)
    {
        free(g_pOldBuf);
    }
}

void ProcessGaussianBlur(PBYTE pBuf, UINT32 width, UINT32 height, UINT32 bpp, UINT8 level)
{
    if (pBuf)
    {
        UINT32 ksize = 4 * level + 1;
        memcpy(g_inputImg.data, pBuf, width * height * 4);
        cv::Mat imgProcess;
        g_inputImg.convertTo(imgProcess, CV_32FC4, 1.0 / 255.0);
        cv::split(imgProcess, g_imgChannel);
        cv::GaussianBlur(g_imgChannel[3], g_mask, cv::Size(ksize, ksize), 0.0, 0.0);
        g_imgChannel[0] = g_imgChannel[0].mul(g_mask);
        g_imgChannel[1] = g_imgChannel[1].mul(g_mask);
        g_imgChannel[2] = g_imgChannel[2].mul(g_mask);
        g_imgChannel[3] = g_mask;
        cv::merge(g_imgChannel, 4, imgProcess);
        imgProcess.convertTo(g_outputImg, CV_8UC4, 255);
        memcpy(pBuf, g_outputImg.data, width * height * bpp);
    }
}