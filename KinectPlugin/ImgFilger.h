#pragma once


#ifndef __IMG_FILTER_H__
#define __IMG_FILTER_H__

#include "opencv2/opencv.hpp"

void InitImgFilter(UINT32 dim, UINT32 bpp);
void DestroyImgFilter();
void ProcessGaussianBlur(PBYTE pBuf, UINT32 width, UINT32 height, UINT32 bpp, UINT8 level);
#endif
