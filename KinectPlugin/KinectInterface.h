#pragma once

#ifndef __KINECT_INTERFACE_H__
#define __KINECT_INTERFACE_H__

#include <Windows.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef KINECTPLUGIN_EXPORTS
#define KINECT_INTERFACE_API __declspec(dllexport) 
#else
#define KINECT_INTERFACE_API __declspec(dllimport) 
#endif

    namespace KinectInterface
    {
        KINECT_INTERFACE_API HANDLE AttachKinect();
        KINECT_INTERFACE_API VOID DetachKinect();
        KINECT_INTERFACE_API VOID EnableImageFilter(BOOL bEnable, UINT8 level);
    }

#ifdef __cplusplus
}
#endif

#endif //__KINECT_INTERFACE_H__
