========================================================================
    DYNAMIC LINK LIBRARY : KinectPlugin Project Overview
========================================================================

AppWizard has created this KinectPlugin DLL for you.

This file contains a summary of what you will find in each of the files that
make up your KinectPlugin application.


KinectPlugin.vcxproj
    This is the main project file for VC++ projects generated using an Application Wizard.
    It contains information about the version of Visual C++ that generated the file, and
    information about the platforms, configurations, and project features selected with the
    Application Wizard.

KinectPlugin.vcxproj.filters
    This is the filters file for VC++ projects generated using an Application Wizard. 
    It contains information about the association between the files in your project 
    and the filters. This association is used in the IDE to show grouping of files with
    similar extensions under a specific node (for e.g. ".cpp" files are associated with the
    "Source Files" filter).

KinectPlugin.cpp
    This is the main DLL source file.

	When created, this DLL does not export any symbols. As a result, it
	will not produce a .lib file when it is built. If you wish this project
	to be a project dependency of some other project, you will either need to
	add code to export some symbols from the DLL so that an export library
	will be produced, or you can set the Ignore Input Library property to Yes
	on the General propert page of the Linker folder in the project's Property
	Pages dialog box.

/////////////////////////////////////////////////////////////////////////////
Other standard files:

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named KinectPlugin.pch and a precompiled types file named StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////
Other notes:

使用方法：
头文件:
#include "KinectInterface.h"

动态链接导入库：
KinectPlugin.lib

参见KinectTest.cpp中的例子：
1.  在合适的位置调用KinectInterface::AttachKinect()，初始化Kinect并且获得人像Texture的shared HANDLE。(line 232)
2.  在UE用于render的device上，调用OpenSharedResource()获得该HANDLE对应的ID3D11Texture2D。(line 414)
3.  调用GetDesc()可以获得该texture的属性。由于Kinect的定义，带有深度信息的texture的原始尺寸为512*424，建议根据GetDesc()
    返回的宽高来创建其他ID3D11Texture2D。(line 419-421)
4.  任意时刻，可以通过对从第2步中得到的ID3D11Texture2D进行CopyResource操作来获得最新图像。(line 499，例子中是在每一帧准备SRV时进行更新)
5.  把CopyResource获得的图像作为SR，可以render至任何RT. (line 500) 这样操作的前提是要先生成一个SRV(line 433)
6.  使用完毕，调用KinectInterface::DetachKinect()释放Kinect。(line 510)
基本流程可以概括为：
- 拿到Kinect Texture的shared handle.
- 用该handle打开ID3D11Texture2D.
- create一份具有相似属性的ID3D11Texture2D，设置正确的flag，并创建该ID3D11Texture2D的ID3D11ShaderResourceView.
- 每次render前，copy Kinect Texture至需要作为SRV的Texture。然后设置SRV，render

图像处理：
EnableImageFilter(BOOL bEnable, UINT8 level);
bEnable: 是否使用滤镜
level: 滤镜级别[0-10]
/////////////////////////////////////////////////////////////////////////////
