// KinectPlugin.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "KinectInterface.h"
#include "inc/DxCommon.h"
#include "Kinect.h"
#include "ImgFilger.h"
#include "Resource.h"

typedef struct _THREAD_DATA
{
    UINT32 width;
    UINT32 height;
    HANDLE hThread;
    DWORD tId;
    HANDLE evExit;
    HANDLE evTimer;
    HANDLE evReady;
    HANDLE hTimer;
    HANDLE hTimerQueue;
    HANDLE hSharedTex;
}THREAD_DATA, *PTHREAD_DATA;

DWORD WINAPI RenderThread(_In_ PVOID pParam);
VOID CALLBACK PerodicTimerCB(PVOID pParam, BOOLEAN bTimerOrWaitFired);

THREAD_DATA g_tData = { 0 };
UINT g_FPS = 60;
HMODULE g_hCurrent = NULL;
UINT32 g_kinectWidth = 512;
UINT32 g_kinectHeight = 424;
UINT32 g_bpp = 4;
BOOL g_filterEnabled = FALSE;
UINT8 g_filterLevel = 1;

KINECT_INTERFACE_API HANDLE KinectInterface::AttachKinect()
{
    HRESULT hr = S_OK;

    InitImgFilter(g_kinectWidth, g_bpp);

    RtlZeroMemory(&g_tData, sizeof(g_tData));
    g_tData.width = g_kinectWidth;
    g_tData.height = g_kinectHeight;
    g_tData.evExit = ::CreateEvent(NULL, TRUE, FALSE, NULL);
    g_tData.evTimer = ::CreateEvent(NULL, TRUE, FALSE, NULL);
    g_tData.evReady = ::CreateEvent(NULL, TRUE, FALSE, NULL);
    ::ResetEvent(g_tData.evExit);
    ::ResetEvent(g_tData.evTimer);
    ::ResetEvent(g_tData.evReady);

    g_tData.hTimerQueue = CreateTimerQueue();
    if (g_tData.hTimerQueue)
    {
        if (CreateTimerQueueTimer(&g_tData.hTimer, g_tData.hTimerQueue,
            (WAITORTIMERCALLBACK)PerodicTimerCB, &g_tData.evTimer, 1000 / g_FPS, 1000 / g_FPS, WT_EXECUTEDEFAULT))
        {
            g_tData.hThread = ::CreateThread(nullptr, 0, RenderThread, &g_tData, 0, &g_tData.tId);
        }
    }
    ::WaitForSingleObjectEx(g_tData.evReady, INFINITE, FALSE);
    return g_tData.hSharedTex;
}

KINECT_INTERFACE_API VOID KinectInterface::DetachKinect()
{
    if(g_tData.evExit) ::SetEvent(g_tData.evExit);
    if(g_tData.evReady) ::SetEvent(g_tData.evReady);
    if(g_tData.hTimerQueue && g_tData.hTimer) ::DeleteTimerQueueTimer(g_tData.hTimerQueue, g_tData.hTimer, NULL);
    if (g_tData.hTimerQueue) ::DeleteTimerQueueEx(g_tData.hTimerQueue, NULL);
    ::WaitForSingleObjectEx(g_tData.hThread, 3000, FALSE);
    SafeCloseHandle(g_tData.evExit);
    SafeCloseHandle(g_tData.evTimer);
    SafeCloseHandle(g_tData.evReady);
    RtlZeroMemory(&g_tData, sizeof(g_tData));
    DestroyImgFilter();
}

KINECT_INTERFACE_API VOID KinectInterface::EnableImageFilter(BOOL bEnable, UINT8 level)
{
    g_filterEnabled = bEnable;
    g_filterLevel = level <= 10 ? level : 10;
}

DWORD WINAPI RenderThread(_In_ PVOID pParam)
{
    PTHREAD_DATA pTData = (PTHREAD_DATA)pParam;

    HRESULT hr = S_OK;
    ID3D11Device *pDx11Dev = NULL;
    ID3D11DeviceContext *pDx11DevCtx = NULL;
    ID3D11Texture2D *pRT = NULL;
    ID3D11RenderTargetView *pRTV = NULL;
    ID3D11Texture2D *pColorFrameSR = NULL;
    ID3D11ShaderResourceView *pColorFrameSRV = NULL;
    ID3D11RenderTargetView *pColorFrameRTV = NULL;
    ID3D11Texture2D *pHumanFrameSR = NULL;
    ID3D11ShaderResourceView *pHumanFrameSRV = NULL;
    ID3D11RenderTargetView *pHumanFrameRTV = NULL;
    ID3D11Texture2D *pTexShared = NULL;
    float ClearColor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };

    D3D_DRIVER_TYPE DriverTypes[] =
    {
        D3D_DRIVER_TYPE_HARDWARE,
        D3D_DRIVER_TYPE_WARP,
        D3D_DRIVER_TYPE_REFERENCE,
    };
    UINT NumDriverTypes = ARRAYSIZE(DriverTypes);
    D3D_FEATURE_LEVEL FeatureLevels[] =
    {
        D3D_FEATURE_LEVEL_11_1,
        D3D_FEATURE_LEVEL_11_0,
    };
    UINT NumFeatureLevels = ARRAYSIZE(FeatureLevels);

    D3D_FEATURE_LEVEL FeatureLevel;

    UINT createDeviceFlags = 0;
#ifdef _DEBUG
    createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif
    for (UINT DriverTypeIndex = 0; DriverTypeIndex < NumDriverTypes; ++DriverTypeIndex)
    {
        hr = ::D3D11CreateDevice(
            NULL, DriverTypes[DriverTypeIndex],
            NULL, createDeviceFlags, FeatureLevels, NumFeatureLevels,
            D3D11_SDK_VERSION, &pDx11Dev, &FeatureLevel, &pDx11DevCtx);
        if (SUCCEEDED(hr))
        {
            break;
        }
    }

    D3D11_TEXTURE2D_DESC texDesc = { 0 };
    if (SUCCEEDED(hr))
    {
        texDesc.Width = pTData->width;
        texDesc.Height = pTData->height;
        texDesc.MipLevels = 1;
        texDesc.ArraySize = 1;
        texDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
        texDesc.SampleDesc.Count = 1;
        texDesc.SampleDesc.Quality = 0;
        texDesc.Usage = D3D11_USAGE_DEFAULT;
        texDesc.BindFlags = D3D11_BIND_RENDER_TARGET;
        texDesc.CPUAccessFlags = 0;
        texDesc.MiscFlags = 0;
        hr = pDx11Dev->CreateTexture2D(&texDesc, NULL, &pRT);
    }
    if (SUCCEEDED(hr))
    {
        hr = pDx11Dev->CreateRenderTargetView(pRT, NULL, &pRTV);
    }

    if(SUCCEEDED(hr))
    {
        texDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
        texDesc.MiscFlags = D3D11_RESOURCE_MISC_SHARED;
        hr = pDx11Dev->CreateTexture2D(&texDesc, NULL, &pTexShared);
    }

    HANDLE hShared = NULL;
    IDXGIResource *pDXGIResource = nullptr;
    if (SUCCEEDED(hr))
    {
        hr = pTexShared->QueryInterface(__uuidof(IDXGIResource), reinterpret_cast<void**>(&pDXGIResource));
    }
    if (SUCCEEDED(hr))
    {
        pDXGIResource->GetSharedHandle(&hShared);
    }
    SafeRelease(pDXGIResource);
    if (hShared)
    {
        pTData->hSharedTex = hShared;
    }

    // Kinect
    IKinectSensor *pKinectSensor = NULL;
    IMultiSourceFrameReader *pMultiSourceFrameReader = NULL;
    ICoordinateMapper *pCoordMapper = NULL;

    IMultiSourceFrame *pMultiSourceFrame = NULL;
    IColorFrameReference *pRefColorFrame = NULL;
    IDepthFrameReference *pRefDepthFrame = NULL;
    IBodyIndexFrameReference *pRefBodyIndexFrame = NULL;
    IColorFrame *pColorFrame = NULL;
    IDepthFrame *pDepthFrame = NULL;
    IBodyIndexFrame *pBodyIndexFrame = NULL;

    if (SUCCEEDED(hr))
    {
        hr = ::GetDefaultKinectSensor(&pKinectSensor);
    }
    if (SUCCEEDED(hr))
    {
        hr = pKinectSensor->Open();
    }

    if (SUCCEEDED(hr))
    {
        hr = pKinectSensor->get_CoordinateMapper(&pCoordMapper);
    }
    if (SUCCEEDED(hr))
    {
        hr = pKinectSensor->OpenMultiSourceFrameReader(FrameSourceTypes_Color | FrameSourceTypes_Depth | FrameSourceTypes_BodyIndex, &pMultiSourceFrameReader);
        pMultiSourceFrameReader->AddRef();
    }

    HANDLE evFrameAvailable = NULL;
    evFrameAvailable = ::CreateEvent(NULL, TRUE, FALSE, NULL);
    ::ResetEvent(evFrameAvailable);

    if (SUCCEEDED(hr))
    {
        hr = pMultiSourceFrameReader->SubscribeMultiSourceFrameArrived((WAITABLE_HANDLE*)&evFrameAvailable);
    }

    IFrameDescription* pFrameDescription = NULL;
    INT cFrameWidth = 0;
    INT cFrameHeight = 0;
    ColorImageFormat cFormat = ColorImageFormat_None;
    UINT cBPP = 0;
    INT dFrameWidth = 0;
    INT dFrameHeight = 0;
    UINT dBPP = 0;
    INT biFrameWidth = 0;
    INT biFrameHeight = 0;
    UINT biBPP = 0;
    ColorSpacePoint *pColorSpacePointer = NULL;

    PBYTE pColorBuf = NULL;
    PBYTE pColorMaskBuf = NULL;
    PUINT16 pDepthBuf = NULL;
    PBYTE pBodyIndexBuf = NULL;
    PBYTE pOutBuf = NULL;
    ::SetEvent(pTData->evReady);

    // Render loop
    while ((SUCCEEDED(hr)) && (::WaitForSingleObjectEx(pTData->evExit, 0, FALSE) == WAIT_TIMEOUT))
    {
        if (::WaitForSingleObjectEx(pTData->evTimer, 1000, FALSE) == WAIT_OBJECT_0)
        {
            if (::WaitForSingleObjectEx(evFrameAvailable, 1000, FALSE) == WAIT_OBJECT_0)
            {
                if (SUCCEEDED(pMultiSourceFrameReader->AcquireLatestFrame(&pMultiSourceFrame)))
                {
                    if (SUCCEEDED(pMultiSourceFrame->get_ColorFrameReference(&pRefColorFrame)))
                    {
                        pRefColorFrame->AcquireFrame(&pColorFrame);
                    }
                    if (SUCCEEDED(pMultiSourceFrame->get_DepthFrameReference(&pRefDepthFrame)))
                    {
                        pRefDepthFrame->AcquireFrame(&pDepthFrame);
                    }
                    if (SUCCEEDED(pMultiSourceFrame->get_BodyIndexFrameReference(&pRefBodyIndexFrame)))
                    {
                        pRefBodyIndexFrame->AcquireFrame(&pBodyIndexFrame);
                    }

                    if (pColorFrame && pDepthFrame && pBodyIndexFrame)
                    {
                        INT frameWidth = 0;
                        INT frameHeight = 0;
                        UINT BPP = 0;
                        pColorFrame->get_FrameDescription(&pFrameDescription);
                        pFrameDescription->get_Width(&frameWidth);
                        pFrameDescription->get_Height(&frameHeight);
                        //pFrameDescription->get_BytesPerPixel(&BPP);
                        if ((frameWidth != cFrameWidth) || (frameHeight != cFrameHeight))
                        {
                            cFrameWidth = frameWidth;
                            cFrameHeight = frameHeight;
                            cBPP = 4;
                            pColorFrame->get_RawColorImageFormat(&cFormat);

                            SafeRelease(pHumanFrameRTV);
                            SafeRelease(pHumanFrameSRV);
                            SafeRelease(pHumanFrameSR);
                            SafeRelease(pColorFrameRTV);
                            SafeRelease(pColorFrameSRV);
                            SafeRelease(pColorFrameSR);

                            D3D11_TEXTURE2D_DESC figureDesc = { 0 };
                            //figureDesc.Width = cFrameWidth;
                            //figureDesc.Height = cFrameHeight;
                            figureDesc.Width = pTData->width;
                            figureDesc.Height = pTData->height;
                            figureDesc.MipLevels = 1;
                            figureDesc.ArraySize = 1;
                            figureDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
                            figureDesc.SampleDesc.Count = 1;
                            figureDesc.SampleDesc.Quality = 0;
                            figureDesc.Usage = D3D11_USAGE_DYNAMIC;
                            figureDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
                            figureDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
                            figureDesc.MiscFlags = 0;
                            if (SUCCEEDED(pDx11Dev->CreateTexture2D(&figureDesc, NULL, &pColorFrameSR)))
                            {
                                pColorFrameSR->GetDesc(&figureDesc);
                                D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
                                srvDesc.Format = figureDesc.Format;
                                srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
                                srvDesc.Texture2D.MostDetailedMip = 0;
                                srvDesc.Texture2D.MipLevels = -1;
                                hr = pDx11Dev->CreateShaderResourceView(pColorFrameSR, &srvDesc, &pColorFrameSRV);
                            }
                            if (SUCCEEDED(hr))
                            {
                                //hr = pDx11Dev->CreateRenderTargetView(pColorFrameSR, NULL, &pColorFrameRTV);
                            }
                            if (SUCCEEDED(pDx11Dev->CreateTexture2D(&figureDesc, NULL, &pHumanFrameSR)))
                            {
                                D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
                                srvDesc.Format = figureDesc.Format;
                                srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
                                srvDesc.Texture2D.MostDetailedMip = 0;
                                srvDesc.Texture2D.MipLevels = -1;
                                hr = pDx11Dev->CreateShaderResourceView(pHumanFrameSR, &srvDesc, &pHumanFrameSRV);
                            }
                            if (SUCCEEDED(hr))
                            {
                                //hr = pDx11Dev->CreateRenderTargetView(pHumanFrameSR, NULL, &pHumanFrameRTV);
                            }

                            SafeFree(pColorBuf);
                            SafeFree(pColorMaskBuf);
                            pColorBuf = (PBYTE)malloc(cFrameWidth * cFrameHeight * 4);
                            if (pColorBuf)
                            {
                                RtlZeroMemory(pColorBuf, cFrameWidth * cFrameHeight * 4);
                            }
                            pColorMaskBuf = (PBYTE)malloc(cFrameWidth * cFrameHeight);
                        }
                        SafeRelease(pFrameDescription);

                        pDepthFrame->get_FrameDescription(&pFrameDescription);
                        pFrameDescription->get_Width(&frameWidth);
                        pFrameDescription->get_Height(&frameHeight);
                        pFrameDescription->get_BytesPerPixel(&BPP);
                        if ((frameWidth != dFrameWidth) || (frameHeight != dFrameHeight) || (BPP != dBPP))
                        {
                            dFrameWidth = frameWidth;
                            dFrameHeight = frameHeight;
                            dBPP = BPP;
                            SafeFree(pDepthBuf);
                            pDepthBuf = (PUINT16)malloc(dFrameWidth * dFrameHeight * dBPP);
                            if (pDepthBuf)
                            {
                                RtlZeroMemory(pDepthBuf, dFrameWidth * dFrameHeight * dBPP);
                            }
                            SafeFree(pOutBuf);
                            pOutBuf = (PBYTE)malloc(dFrameWidth * dFrameHeight * cBPP);
                            if (pOutBuf)
                            {
                                RtlZeroMemory(pOutBuf, dFrameWidth * dFrameHeight * cBPP);
                            }
                            SafeFree(pColorSpacePointer);
                            pColorSpacePointer = (ColorSpacePoint*)malloc(dFrameWidth * dFrameHeight * sizeof(ColorSpacePoint));
                            if (pColorSpacePointer)
                            {
                                RtlZeroMemory(pColorSpacePointer, dFrameWidth * dFrameHeight * sizeof(ColorSpacePoint));
                            }
                        }
                        SafeRelease(pFrameDescription);

                        pBodyIndexFrame->get_FrameDescription(&pFrameDescription);
                        pFrameDescription->get_Width(&frameWidth);
                        pFrameDescription->get_Height(&frameHeight);
                        pFrameDescription->get_BytesPerPixel(&BPP);
                        if ((frameWidth != biFrameWidth) || (frameHeight != biFrameHeight) || (BPP != biBPP))
                        {
                            biFrameWidth = frameWidth;
                            biFrameHeight = frameHeight;
                            biBPP = BPP;
                            SafeFree(pBodyIndexBuf);
                            pBodyIndexBuf = (PBYTE)malloc(biFrameWidth * biFrameHeight * biBPP);
                            if (pBodyIndexBuf)
                            {
                                RtlZeroMemory(pBodyIndexBuf, biFrameWidth * biFrameHeight * biBPP);
                            }
                        }
                        SafeRelease(pFrameDescription);

                        if (pColorFrameSR && pColorFrameSRV && //pColorFrameRTV && 
                            pHumanFrameSR && pHumanFrameSRV && //pHumanFrameRTV &&
                            pColorBuf && pColorMaskBuf && 
                            pDepthBuf && pColorSpacePointer && 
                            pBodyIndexBuf && pOutBuf)
                        {
                            if (cFormat == ColorImageFormat_Bgra)
                            {
                                hr = pColorFrame->CopyRawFrameDataToArray(cFrameWidth * cFrameHeight * cBPP, pColorBuf);
                            }
                            else
                            {
                                hr = pColorFrame->CopyConvertedFrameDataToArray(cFrameWidth * cFrameHeight * cBPP, pColorBuf, ColorImageFormat_Bgra);
                            }
                            hr = pDepthFrame->CopyFrameDataToArray(dFrameWidth * dFrameHeight, pDepthBuf);
                            hr = pBodyIndexFrame->CopyFrameDataToArray(biFrameWidth * biFrameHeight * biBPP, pBodyIndexBuf);

                            hr = pCoordMapper->MapDepthFrameToColorSpace(dFrameWidth * dFrameHeight, pDepthBuf, dFrameWidth * dFrameHeight, pColorSpacePointer);
                            RtlZeroMemory(pOutBuf, dFrameWidth * dFrameHeight * cBPP);

                            //pDx11DevCtx->ClearRenderTargetView(pColorFrameRTV, ClearColor);
                            //pDx11DevCtx->ClearRenderTargetView(pHumanFrameRTV, ClearColor);
                            RtlZeroMemory(pColorMaskBuf, cFrameWidth * cFrameHeight);

                            for (INT y = 0; y < dFrameHeight; y++)
                            {
                                for (INT x = 0; x < dFrameWidth; x++)
                                {
                                    if (pBodyIndexBuf[y * dFrameWidth + x] != 0xFF)
                                    {
                                        INT colorX = (INT)floor(pColorSpacePointer[y * dFrameWidth + x].X + 0.5);
                                        INT colorY = (INT)floor(pColorSpacePointer[y * dFrameWidth + x].Y + 0.5);
                                        if((colorX >= 0) && (colorX < cFrameWidth) && (colorY >= 0) && (colorY < cFrameHeight))
                                        {
                                            ((PUINT32)pOutBuf)[y * dFrameWidth + x] = ((PUINT32)pColorBuf)[colorY * cFrameWidth + colorX];
                                        }
                                    }
                                }
                            }
                            if (g_filterEnabled)
                            {
                                ProcessGaussianBlur(pOutBuf, dFrameWidth, dFrameHeight, cBPP, g_filterLevel);
                            }
                            D3D11_MAPPED_SUBRESOURCE mappedResource = { 0 };
                            if (SUCCEEDED(pDx11DevCtx->Map(pColorFrameSR, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource)))
                            {
                                RtlCopyMemory(mappedResource.pData, pOutBuf, dFrameWidth * dFrameHeight * cBPP);
                            }
                            pDx11DevCtx->Unmap(pColorFrameSR, 0);
                            pDx11DevCtx->CopyResource(pTexShared, pColorFrameSR);
                            pDx11DevCtx->Flush();
                        }
                    }

                    SafeRelease(pColorFrame);
                    SafeRelease(pDepthFrame);
                    SafeRelease(pBodyIndexFrame);
                    SafeRelease(pRefColorFrame);
                    SafeRelease(pRefDepthFrame);
                    SafeRelease(pRefBodyIndexFrame);
                }
                SafeRelease(pMultiSourceFrame);
            }
            Sleep(16);
            ::ResetEvent(pTData->evTimer);
        }
    }

    // Clear
    if (pOutBuf && pColorFrameSR)
    {
        RtlZeroMemory(pOutBuf, dFrameWidth * dFrameHeight * cBPP);
        D3D11_MAPPED_SUBRESOURCE mappedResource = { 0 };
        if (SUCCEEDED(pDx11DevCtx->Map(pColorFrameSR, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource)))
        {
            RtlCopyMemory(mappedResource.pData, pOutBuf, dFrameWidth * dFrameHeight * cBPP);
        }
        pDx11DevCtx->Unmap(pColorFrameSR, 0);
        pDx11DevCtx->CopyResource(pTexShared, pColorFrameSR);
        pDx11DevCtx->Flush();
    }

    SafeFree(pOutBuf);
    SafeFree(pBodyIndexBuf);
    SafeFree(pColorSpacePointer);
    SafeFree(pDepthBuf);
    SafeFree(pColorBuf);
    SafeFree(pColorMaskBuf);
    SafeCloseHandle(evFrameAvailable);

    SafeRelease(pFrameDescription);
    SafeRelease(pColorFrame);
    SafeRelease(pDepthFrame);
    SafeRelease(pBodyIndexFrame);
    SafeRelease(pRefColorFrame);
    SafeRelease(pRefDepthFrame);
    SafeRelease(pRefBodyIndexFrame);
    SafeRelease(pMultiSourceFrame);

    SafeRelease(pCoordMapper);
    SafeRelease(pMultiSourceFrameReader);
    if (pKinectSensor)
    {
        pKinectSensor->Close();
    }
    SafeRelease(pKinectSensor);

    SafeRelease(pTexShared);
    SafeRelease(pHumanFrameRTV);
    SafeRelease(pHumanFrameSRV);
    SafeRelease(pHumanFrameSR);
    SafeRelease(pColorFrameRTV);
    SafeRelease(pColorFrameSRV);
    SafeRelease(pColorFrameSR);
    SafeRelease(pRTV);
    SafeRelease(pRT);
    SafeRelease(pDx11DevCtx);
    SafeRelease(pDx11Dev);
    return 0;
}

VOID CALLBACK PerodicTimerCB(PVOID pParam, BOOLEAN bTimerOrWaitFired)
{
    HANDLE ev = *(PHANDLE)pParam;
    ::SetEvent(ev);
}
