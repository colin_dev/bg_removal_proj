#pragma once
#include "VirtualContent.h"
//------------------------------------------------------------------------------
// Forward Declarations
//------------------------------------------------------------------------------
// The class managing the output pin
class CVirtualStream;


//------------------------------------------------------------------------------
// Class CVirtualContent
//
// This is the main class for the virtual content filter. It inherits from
// CSource, the DirectShow base class for source filters.
//------------------------------------------------------------------------------
class CVirtualContent : public CSource
{
public:

    // The only allowed way to create virtual content!
    static CUnknown * WINAPI CreateInstance(LPUNKNOWN lpunk, HRESULT *phr);

private:

    // It is only allowed to to create these objects with CreateInstance
    CVirtualContent(LPUNKNOWN lpunk, HRESULT *phr);

}; // CVirtualContent


//------------------------------------------------------------------------------
// Class CVirtualStream
//
// This class implements the stream which is used to output the virtual content
// data from the source filter. It inherits from DirectShows's base
// CSourceStream class.
//------------------------------------------------------------------------------
class CVirtualStream : public CSourceStream
{

public:

    CVirtualStream(HRESULT *phr, CVirtualContent *pParent, LPCWSTR pPinName);
    ~CVirtualStream();

    // plots virtual content into the supplied video frame
    HRESULT FillBuffer(IMediaSample *pms);

    // Ask for buffers of the size appropriate to the agreed media type
    HRESULT DecideBufferSize(IMemAllocator *pIMemAlloc,
        ALLOCATOR_PROPERTIES *pProperties);

    // Set the agreed media type, and set up the necessary content parameters
    HRESULT SetMediaType(const CMediaType *pMediaType);

    // Because we calculate the content there is no reason why we
    // can't calculate it in any one of a set of formats...
    HRESULT CheckMediaType(const CMediaType *pMediaType);
    HRESULT GetMediaType(int iPosition, CMediaType *pmt);

    // Resets the stream time to zero
    HRESULT OnThreadCreate(void);

    // Quality control notifications sent to us
    STDMETHODIMP Notify(IBaseFilter * pSender, Quality q);

private:

    int m_iImageHeight;                 // The current image height
    int m_iImageWidth;                  // And current image width
    int m_iRepeatTime;                  // Time in msec between frames
    const int m_iDefaultRepeatTime;     // Initial m_iRepeatTime

    BYTE m_BallPixel[4];                // Represents one coloured ball
    int m_iPixelSize;                   // The pixel size in bytes
    PALETTEENTRY m_Palette[256];        // The optimal palette for the image

    CCritSec m_cSharedState;            // Lock on m_rtSampleTime and m_Content
    CRefTime m_rtSampleTime;            // The time stamp for each sample
    CContent *m_Content;                      // The current ball object

    // set up the palette appropriately
    enum Colour { Red, Blue, Green, Yellow };
    HRESULT SetPaletteEntries(Colour colour);

}; // CVirtualStream
