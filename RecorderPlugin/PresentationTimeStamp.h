#pragma once

#include <strmif.h>
#include "inc/DxCommon.h"

class PresentationTimeStamp
{
public:
    PresentationTimeStamp();
    ~PresentationTimeStamp();
    VOID GetPTS(REFERENCE_TIME &pts, REFERENCE_TIME &ptsNonStop);
    VOID GetLastPTS(REFERENCE_TIME &lastPts);
    VOID SaveLastPTS(REFERENCE_TIME lastPts);
    VOID ReStartPTS();
    VOID PausePTS();
    VOID ResumePTS();
private:
    static VOID CALLBACK PseudoClock(PVOID pParam, BOOLEAN bTimerOrWaitFired);

    CONST UINT32 m_clkInterval = 1;
    CONST REFERENCE_TIME REFTIMES_PER_SEC = 10000000;

    CRITICAL_SECTION m_cs;
    HANDLE m_hTimer;
    HANDLE m_hTimerQueue;
    HANDLE m_evReset;
    HANDLE m_evActive;

    LARGE_INTEGER m_qpfFreq;
    LARGE_INTEGER m_qpcStart;
    LARGE_INTEGER m_qpcPauseStart;
    LARGE_INTEGER m_qpcPauseDuration;
    REFERENCE_TIME m_pts;
    REFERENCE_TIME m_lastPts;
    REFERENCE_TIME m_ptsNonPause;
};

