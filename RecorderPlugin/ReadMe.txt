========================================================================
    DYNAMIC LINK LIBRARY : RecorderPlugin Project Overview
========================================================================

AppWizard has created this RecorderPlugin DLL for you.

This file contains a summary of what you will find in each of the files that
make up your RecorderPlugin application.


RecorderPlugin.vcxproj
    This is the main project file for VC++ projects generated using an Application Wizard.
    It contains information about the version of Visual C++ that generated the file, and
    information about the platforms, configurations, and project features selected with the
    Application Wizard.

RecorderPlugin.vcxproj.filters
    This is the filters file for VC++ projects generated using an Application Wizard. 
    It contains information about the association between the files in your project 
    and the filters. This association is used in the IDE to show grouping of files with
    similar extensions under a specific node (for e.g. ".cpp" files are associated with the
    "Source Files" filter).

RecorderPlugin.cpp
    This is the main DLL source file.

	When created, this DLL does not export any symbols. As a result, it
	will not produce a .lib file when it is built. If you wish this project
	to be a project dependency of some other project, you will either need to
	add code to export some symbols from the DLL so that an export library
	will be produced, or you can set the Ignore Input Library property to Yes
	on the General propert page of the Linker folder in the project's Property
	Pages dialog box.

/////////////////////////////////////////////////////////////////////////////
Other standard files:

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named RecorderPlugin.pch and a precompiled types file named StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////
Other notes:

使用方法：
头文件:
#include "RecorderInterface.h"

动态链接到入库：
RecorderPlugin.lib

参见RecorderTest.cpp中的例子：
1. 创建一块具有 D3D11_RESOURCE_MISC_SHARED 属性的 ID3D11Texture2D。(line 577)
可根据需要自行调整大小或格式，或者直接采用和backbuffer相同格式以便于从back buffer复制。(line 708)
拿到该 ID3D11Texture2D 的shared handle。(line 583)
2. 准备好所有用于录像的参数，调用接口RecorderInterface::StartRecording。(line 641)
3. 在每一帧render完成后，调用接口RecorderInterface::NotifyFrameUpdate通知记录此帧。(line 709)
如果是直接复制自back buffer，需使用类似line 708的方法将back buffer拷贝至用于share给recorder的buffer。
如果是自行创建的ID3D11Texture2D，也需要根据需要更新内容。
4. RecorderPlugin会根据NotifyFrameUpdate的通知，根据被通知时的实际时间戳记录此帧。
5. 若要停止记录，需调用接口RecorderInterface::StopRecording。(line 719)
6. 使用RecorderInterface::PauseRecording()来暂停记录。(line 771)
7. 使用RecorderInterface::ResumeRecording()来暂停记录。(line 779)
8. 使用RecorderInterface::GetRecordStatus()获得当前状态。(line 641, 769, 777)

状态机：
目前recording有三种状态：STOPPED, STARTTED, PAUSED。只有合法的状态转换，相应的API才会生效。请注意内部状态的维护。

STOPPED
>>>>STARTTED,StartRecording()

STARTTED
>>>>PAUSED,PauseRecording()
>>>>STOPPED,StopRecording()

PAUSED
>>>>STARTTED,ResumeRecording()
>>>>STOPPED,StopRecording()

RTSP协议支持：
1. StartRecording()接口增加一个参数bStreaming用来指定使用录像模式还是串流模式。true表示使用串流模式。
2. StartRecording()的第三个参数pFileName用于指定串流链接，如RecorderTest中指定了"live"，则表示
用于播放的地址为rtsp://w.x.y.z:8554/live. IP地址可以时本地回送地址127.0.0.1，或可被网络访问的地址。
3. RTSP状态时，不支持pause与resume操作。


关于引擎插件的集成说明：
1. 引擎必须使用D3D11加速。
2. 引擎插件必须能够创建要共享给RecorderPlugin的ID3D11Texture2D，并获得shared handle传递给RecorderPlugin。详见上面第一点。
3. 引擎插件需使用C++开发，并且链接至RecorderPlugin。如使用C#开发，可能需要PInvoke完成调用，正确性未验证。


/////////////////////////////////////////////////////////////////////////////
