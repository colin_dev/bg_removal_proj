/**********
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation; either version 3 of the License, or (at your
option) any later version. (See <http://www.gnu.org/copyleft/lesser.html>.)

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
**********/
// "liveMedia"
// Copyright (c) 1996-2018 Live Networks, Inc.  All rights reserved.
// A source object for AAC audio files in ADTS format
// Implementation

#include "stdafx.h"
#include "ADTSAudioRingBufferSource.hh"
#include "InputFile.hh"
#include <GroupsockHelper.hh>
#include <assert.h>

static unsigned const samplingFrequencyTable[16] = {
  96000, 88200, 64000, 48000,
  44100, 32000, 24000, 22050,
  16000, 12000, 11025, 8000,
  7350, 0, 0, 0
};

ADTSAudioRingBufferSource*
ADTSAudioRingBufferSource::createNew(UsageEnvironment& env, 
    UINT32 max_frame_size,
    UINT32 avg_bps,
    UINT32 profile,
    UINT32 sampling_frequency_index,
    UINT32 channel_configuration) {
  return new ADTSAudioRingBufferSource(env, max_frame_size, avg_bps, profile,
      sampling_frequency_index, channel_configuration);
}

void ADTSAudioRingBufferSource::feedFrame(PBYTE pCurFrame, unsigned frameSize)
{
    if (frameSize >= fBufferSize)
    {
        assert(0);
        return;
    }
    if (wptr + frameSize > fBufferSize)
    {
        eor = wptr;
        wptr = 0;
    }
    RtlCopyMemory(pBuffer + wptr, pCurFrame, frameSize);
    wptr += frameSize;
}

ADTSAudioRingBufferSource
::ADTSAudioRingBufferSource(UsageEnvironment& env, UINT32 maxBufferSize, UINT32 avg_bps, UINT32 profile,
    UINT32 samplingFrequencyIndex, UINT32 channelConfiguration)
  : FramedSource(env) {
    fBufferSize = maxBufferSize;
    pBuffer = (PBYTE)malloc(fBufferSize);
    wptr = 0;
    rptr = 0;
    eor = fBufferSize;
  fSamplingFrequency = samplingFrequencyTable[samplingFrequencyIndex];
  fNumChannels = channelConfiguration == 0 ? 2 : channelConfiguration;
  fuSecsPerFrame
    = (1024/*samples-per-frame*/*1000000) / fSamplingFrequency/*samples-per-second*/;

  // Construct the 'AudioSpecificConfig', and from it, the corresponding ASCII string:
  unsigned char audioSpecificConfig[2];
  u_int8_t const audioObjectType = profile + 1;
  audioSpecificConfig[0] = (audioObjectType<<3) | (samplingFrequencyIndex>>1);
  audioSpecificConfig[1] = (samplingFrequencyIndex<<7) | (channelConfiguration<<3);
  sprintf_s(fConfigStr, 5, "%02X%02x", audioSpecificConfig[0], audioSpecificConfig[1]);
}

ADTSAudioRingBufferSource::~ADTSAudioRingBufferSource() {
    free(pBuffer);
}

// Note: We should change the following to use asynchronous file reading, #####
// as we now do with ByteStreamFileSource. #####
void ADTSAudioRingBufferSource::doGetNextFrame() {

    BOOL bResetRptr = false;
    UINT32 fTotalFrameSize = 0;
    if ((pBuffer[rptr] != 0xFF) || ((pBuffer[rptr + 1] & 0xF0) != 0xF0))
    {
        rptr = 0;
    }
    if ((pBuffer[rptr] == 0xFF) && ((pBuffer[rptr + 1] & 0xF0) == 0xF0))
    {
        fTotalFrameSize = ((pBuffer[rptr + 3] & 0x03) << 11) | (pBuffer[rptr + 4] << 3) | ((pBuffer[rptr + 5] & 0xE0) >> 5);
        fFrameSize = fTotalFrameSize - 7;
        memmove(fTo, pBuffer + rptr + 7, fFrameSize);
        rptr += fTotalFrameSize;
    }

  fNumTruncatedBytes = 0;

  // Set the 'presentation time':
  if (fPresentationTime.tv_sec == 0 && fPresentationTime.tv_usec == 0) {
    // This is the first frame, so use the current time:
    gettimeofday(&fPresentationTime, NULL);
  } else {
    // Increment by the play time of the previous frame:
    unsigned uSeconds = fPresentationTime.tv_usec + fuSecsPerFrame;
    fPresentationTime.tv_sec += uSeconds/1000000;
    fPresentationTime.tv_usec = uSeconds%1000000;
  }

  fDurationInMicroseconds = fuSecsPerFrame;

  // Switch to another task, and inform the reader that he has data:
  nextTask() = envir().taskScheduler().scheduleDelayedTask(0,
				(TaskFunc*)FramedSource::afterGetting, this);
}
