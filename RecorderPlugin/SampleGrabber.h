#pragma once

#ifndef __SAMPLE_GRABBER_H__
#define __SAMPLE_GRABBER_H__

#include <Mfidl.h>

typedef void(*PFN_ProcessSample)(
    DWORD dwSampleFlags,
    LONGLONG llSampleTime, 
    LONGLONG llSampleDuration, 
    const BYTE * pSampleBuffer,
    DWORD dwSampleSize, 
    IMFAttributes *pAttributes);

class SampleGrabberCB : public IMFSampleGrabberSinkCallback2
{
    long m_cRef;

    SampleGrabberCB() : m_cRef(1) {}

public:
    static HRESULT CreateInstance(SampleGrabberCB **ppCB);

    // IUnknown methods
    STDMETHODIMP QueryInterface(REFIID iid, void** ppv);
    STDMETHODIMP_(ULONG) AddRef();
    STDMETHODIMP_(ULONG) Release();

    // IMFClockStateSink methods
    STDMETHODIMP OnClockStart(MFTIME hnsSystemTime, LONGLONG llClockStartOffset);
    STDMETHODIMP OnClockStop(MFTIME hnsSystemTime);
    STDMETHODIMP OnClockPause(MFTIME hnsSystemTime);
    STDMETHODIMP OnClockRestart(MFTIME hnsSystemTime);
    STDMETHODIMP OnClockSetRate(MFTIME hnsSystemTime, float flRate);

    // IMFSampleGrabberSinkCallback methods
    STDMETHODIMP OnSetPresentationClock(IMFPresentationClock* pClock);
    STDMETHODIMP OnProcessSample(REFGUID guidMajorMediaType, DWORD dwSampleFlags,
        LONGLONG llSampleTime, LONGLONG llSampleDuration, const BYTE * pSampleBuffer,
        DWORD dwSampleSize);
    STDMETHODIMP OnProcessSampleEx(REFGUID guidMajorMediaType, DWORD dwSampleFlags,
        LONGLONG llSampleTime, LONGLONG llSampleDuration, const BYTE * pSampleBuffer,
        DWORD dwSampleSize, IMFAttributes *pAttributes);
    STDMETHODIMP OnShutdown();
    void RegisterProcessSampleCallback(PFN_ProcessSample pfn);
private:
    PFN_ProcessSample m_cbProcessSample;
};

#endif //__SAMPLE_GRABBER_H__