// RecorderPlugin.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "RecorderInterface.h"
#include "inc/DxCommon.h"

#include <string>

#include <Shlwapi.h>

// DShow
#include <Dshow.h>

// MF
#include <mfapi.h>
#include <mfidl.h>
#include <Mfreadwrite.h>
#include <mferror.h>
#include <codecapi.h>

// WSAPI
#include <Mmdeviceapi.h>
#include <Audioclient.h>

// PTS
#include "PresentationTimeStamp.h"

// Sample Grabber
#include "SampleGrabber.h"

// live555
#include <liveMedia.hh>
#include <BasicUsageEnvironment.hh>
#include <GroupsockHelper.hh>
#include "ByteStreamRingBufferSource.h"
#include "ADTSAudioRingBufferSource.hh"

typedef struct _THREAD_DATA
{
    BOOL bStreaming;
}THREAD_DATA, *PTHREAD_DATA;
typedef struct _RTP_TASK_THREAD_DATA
{
    HANDLE hThread;
    DWORD tId;
    UsageEnvironment *pEnv;
    char evStop;
}RTP_TASK_THREAD_DATA, *PRTP_TASK_THREAD_DATA;

HANDLE g_hRT = NULL;
std::wstring g_VideoFileName;
std::wstring g_VideoFileNameA;
std::wstring g_VideoFileNameV;
std::wstring g_audiologFileName;
FILE *g_fpAudioLog;
UINT32 g_videoWidth = 0;
UINT32 g_videoHeight = 0;
UINT32 g_videoBitRate = 0;
UINT32 g_videoFps = 0;
FLOAT g_fpsRatio = 1.05f;

THREAD_DATA g_videoTData = { 0 };
double g_qpfPeroid = 0.0;
HANDLE g_evAudioReady = NULL;
HANDLE g_evVideoReady = NULL;
HANDLE g_evWorkThreadExit = NULL;
HANDLE g_evFrameUpdate = NULL;
HANDLE g_hRecordVideoWorkThread = NULL;
HANDLE g_hRecordAudioWorkThread = NULL;

ID3D11Device *g_pDx11Dev = NULL;
ID3D11DeviceContext *g_pDx11DevCtx = NULL;
ID3D10Multithread *g_pDxMultiThread = NULL;
ID3D11Texture2D *g_pParentSharedRTTex = NULL;
ID3D11Texture2D *g_pVideoInputTex = NULL;
ID3D11Texture2D *g_pVideoOutputTex = NULL;
ID3D11VideoDevice *g_pDx11VideoDev = NULL;
ID3D11VideoContext *g_pDx11VideoDevCtx = NULL;
ID3D11VideoProcessor *g_pDx11VideoProcessor = NULL;
ID3D11VideoProcessorInputView *g_pDx11VideoInputView = NULL;
ID3D11VideoProcessorOutputView *g_pDx11VideoOutputView = NULL;

CONST REFERENCE_TIME REFTIMES_PER_SEC = 10000000;
const BOOL USE_LOW_LATENCY_MODE = TRUE;
const BOOL DISABLE_CONVERSION = FALSE;
const BOOL USE_HW_MFT = TRUE;
const GUID DEFAULT_OUTPUT_CONTAINER_TYPE = MFTranscodeContainerType_MPEG4;
const GUID DEFAULT_VIDEO_IN_FORMAT = MFVideoFormat_NV12;
const GUID DEFAULT_VIDEO_OUT_FORMAT = MFVideoFormat_H264;
const GUID DEFAULT_AUDIO_IN_FORMAT = MFAudioFormat_PCM;
const GUID DEFAULT_AUDIO_OUT_FORMAT = MFAudioFormat_AAC;
const UINT32 DEFAULT_AUDIO_OUT_PROFILE_LEVEL = 0x29;
const UINT32 DEFAULT_AUDIO_OUT_BITS_PER_SAMPLE = 16;
const UINT32 DEFAULT_AUDIO_OUT_SAMPLES_PER_SECOND = 44100;
const UINT32 DEFAULT_AUDIO_OUT_NUM_CHANNELS = 2;
const UINT32 DEFAULT_AUDIO_OUT_AVG_BYTES_PER_SECOND = 20000;
const UINT32 DEFAULT_AUDIO_OUT_AVG_BITRATE = 160000;

BOOL g_bLiveStreaming = FALSE;
IMFSinkWriter *g_pMFAIOSinkWriter = NULL;
SampleGrabberCB *g_pVideoSampleGrabber = NULL;
SampleGrabberCB *g_pAudioSampleGrabber = NULL;
IMFMediaSink *g_pVideoSink = NULL;
IMFMediaSink *g_pAudioSink = NULL;
IMFSinkWriter *g_pMFVideoSinkWriter = NULL;
IMFSinkWriter *g_pMFAudioSinkWriter = NULL;
DWORD g_videoStreamIndex = 0;
DWORD g_audioStreamIndex = 0;
REFERENCE_TIME g_ptsVideo = 0;
REFERENCE_TIME g_ptsAudioNext = 0;
IMMDevice *g_pIMMDev = NULL;
IAudioClient *g_pAudioClient = NULL;
REFERENCE_TIME g_hnsDefaultDevicePeriod = 0;
IAudioCaptureClient *g_pAudioCaptureClient = NULL;
WAVEFORMATEX *g_pWaveFmt = NULL;
UINT32 g_AudioBufFrameCount = 0;
PBYTE g_pAudioBuf = NULL;
UINT32 g_MaxAudioBufSize = 0xA00000;

HANDLE g_hVideoThreadTask = NULL;
HANDLE g_hAudioThreadTask = NULL;

HANDLE g_evRecordStart = NULL;
HANDLE g_evRecordActive = NULL;
CRITICAL_SECTION g_csDelayPause;
LARGE_INTEGER g_qpcPrevVideo = { 0 };
LARGE_INTEGER g_qpcAudioStart = { 0 };

PresentationTimeStamp g_audioPtsHelper;

RecorderInterface::RECORD_STATUS g_Status = RecorderInterface::RECORD_STATUS_STOPPED;

TaskScheduler *g_RTPScheduler;
UsageEnvironment *g_pRTPUsageEnv;
H264VideoStreamFramer *g_pRTPVideoSource;
ADTSAudioRingBufferSource *g_pRTPAudioSource;
RTPSink *g_pRTPVideoSink;
RTPSink *g_pRTPAudioSink;
RTCPInstance *g_pRTCPVideo;
RTCPInstance *g_pRTCPAudio;
Groupsock *g_pRTPVideoGroupsock;
Groupsock *g_pRTCPVideoGroupsock;
Groupsock *g_pRTPAudioGroupsock;
Groupsock *g_pRTCPAudioGroupsock;
RTSPServer *g_pRTSPServer;
ServerMediaSession *g_pRTSPServerMediaSession;
HANDLE g_hRTPTaskSchedulerThread = NULL;
RTP_TASK_THREAD_DATA g_RTPTData;
ByteStreamRingBufferSource *g_pVideoRB;

#define USE_SSM 1
u_int8_t *g_pVideoCache = NULL;
PBYTE g_pAudioFrame = NULL;
const float COMPRESSION_RATIO = 0.07f;
const UINT32 MILLI_PER_SECOND = 1000;
const UINT32 CACHE_TIME_MS = 2000;
const UINT32 DELAY_START_TIME_MS = 500;
const UINT32 RGB32_BPP = 4;
UINT32 g_videoCacheSize = 0;
UINT32 g_audioCacheSize = 0;

HANDLE g_hTimer = NULL;
HANDLE g_hTimerQueue = NULL;

FILE *pRawFile = NULL;

BOOL g_bSplitAV = FALSE;
BOOL g_bWithAudio = TRUE;
BOOL g_bMute = FALSE;

typedef struct _adts_header
{
    BYTE adts[7];
}adts_header, *p_adts_header;

adts_header adtsH = { 0 };

const DWORD MP4FreqList[] = { 96000, 88200, 64000, 48000, 44100, 32000, 24000, 22050, 16000, 12000, 11025, 8000, 7350, 0, 0, 0 };
UCHAR FindFreqIndex(const DWORD samplingFreq)
{
    UCHAR freqIdxMP4 = 0;
    for (UINT idx = 0; idx < ARRAYSIZE(MP4FreqList); idx++)
    {
        if (samplingFreq == MP4FreqList[idx])
        {
            freqIdxMP4 = idx;
            break;
        }
    }
    return freqIdxMP4;

}

HRESULT InitDx11Device()
{
    HRESULT hr = S_OK;

    D3D_DRIVER_TYPE DriverTypes[] =
    {
        D3D_DRIVER_TYPE_HARDWARE,
        D3D_DRIVER_TYPE_WARP,
        D3D_DRIVER_TYPE_REFERENCE,
    };
    UINT NumDriverTypes = ARRAYSIZE(DriverTypes);
    D3D_FEATURE_LEVEL FeatureLevels[] =
    {
        D3D_FEATURE_LEVEL_11_1,
        D3D_FEATURE_LEVEL_11_0,
    };
    UINT NumFeatureLevels = ARRAYSIZE(FeatureLevels);

    D3D_FEATURE_LEVEL FeatureLevel;

    UINT createDeviceFlags = 0;
#ifdef _DEBUG
    createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif
    for (UINT DriverTypeIndex = 0; DriverTypeIndex < NumDriverTypes; ++DriverTypeIndex)
    {
        hr = ::D3D11CreateDevice(
            NULL, DriverTypes[DriverTypeIndex],
            NULL, createDeviceFlags, FeatureLevels, NumFeatureLevels,
            D3D11_SDK_VERSION, &g_pDx11Dev, &FeatureLevel, &g_pDx11DevCtx);
        if (SUCCEEDED(hr))
        {
            break;
        }
    }

    if (SUCCEEDED(hr))
    {
        hr = g_pDx11Dev->QueryInterface(__uuidof(ID3D10Multithread), (void **)&g_pDxMultiThread);
    }
    if (SUCCEEDED(hr))
    {
        g_pDxMultiThread->SetMultithreadProtected(TRUE);
    }

    if (SUCCEEDED(hr))
    {
        hr = g_pDx11Dev->OpenSharedResource(g_hRT, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&g_pParentSharedRTTex));
    }

    D3D11_TEXTURE2D_DESC descParent = { 0 };
    if (SUCCEEDED(hr))
    {
        g_pParentSharedRTTex->GetDesc(&descParent);
        descParent.MiscFlags = 0;
    }

    if (SUCCEEDED(hr))
    {
        hr = g_pDx11Dev->CreateTexture2D(&descParent, NULL, &g_pVideoInputTex);
    }
    if (SUCCEEDED(hr))
    {
        descParent.Width = g_videoWidth;
        descParent.Height = g_videoHeight;
        descParent.Format = DXGI_FORMAT_NV12;
        hr = g_pDx11Dev->CreateTexture2D(&descParent, NULL, &g_pVideoOutputTex);
    }

    if (SUCCEEDED(hr))
    {
        hr = g_pDx11Dev->QueryInterface(__uuidof(ID3D11VideoDevice), (void **)&g_pDx11VideoDev);
    }

    if (SUCCEEDED(hr))
    {
        hr = g_pDx11DevCtx->QueryInterface(__uuidof(ID3D11VideoContext), (void **)&g_pDx11VideoDevCtx);
    }

    D3D11_VIDEO_PROCESSOR_CONTENT_DESC contentDesc = {
        D3D11_VIDEO_FRAME_FORMAT_PROGRESSIVE,
        { 1, 1 }, descParent.Width, descParent.Height,
        { 1, 1 }, descParent.Width, descParent.Height,
        D3D11_VIDEO_USAGE_PLAYBACK_NORMAL
    };
    ID3D11VideoProcessorEnumerator *pVideoProcessorEnumerator = NULL;
    if (SUCCEEDED(hr))
    {
        hr = g_pDx11VideoDev->CreateVideoProcessorEnumerator(&contentDesc, &pVideoProcessorEnumerator);
    }

    if (SUCCEEDED(hr))
    {
        hr = g_pDx11VideoDev->CreateVideoProcessor(pVideoProcessorEnumerator, 0, &g_pDx11VideoProcessor);
    }

    D3D11_VIDEO_PROCESSOR_INPUT_VIEW_DESC inputViewDesc;
    inputViewDesc.FourCC = 0;
    inputViewDesc.ViewDimension = D3D11_VPIV_DIMENSION_TEXTURE2D;
    inputViewDesc.Texture2D.ArraySlice = 0;
    inputViewDesc.Texture2D.MipSlice = 0;

    if (SUCCEEDED(hr))
    {
        hr = g_pDx11VideoDev->CreateVideoProcessorInputView(g_pVideoInputTex, pVideoProcessorEnumerator, &inputViewDesc, &g_pDx11VideoInputView);
    }

    D3D11_VIDEO_PROCESSOR_OUTPUT_VIEW_DESC outputViewDesc;
    outputViewDesc.ViewDimension = D3D11_VPOV_DIMENSION_TEXTURE2D;
    outputViewDesc.Texture2D.MipSlice = 0;
    if (SUCCEEDED(hr))
    {
        hr = g_pDx11VideoDev->CreateVideoProcessorOutputView(g_pVideoOutputTex, pVideoProcessorEnumerator, &outputViewDesc, &g_pDx11VideoOutputView);
    }

    SafeRelease(pVideoProcessorEnumerator);

    return hr;
}

void CleanupDx11Device()
{
    SafeRelease(g_pDx11VideoOutputView);
    SafeRelease(g_pDx11VideoInputView);
    SafeRelease(g_pDx11VideoProcessor);
    SafeRelease(g_pDx11VideoDevCtx);
    SafeRelease(g_pDx11VideoDev);
    SafeRelease(g_pVideoOutputTex);
    SafeRelease(g_pVideoInputTex);
    SafeRelease(g_pParentSharedRTTex);
    SafeRelease(g_pDxMultiThread);
    SafeRelease(g_pDx11DevCtx);
    SafeRelease(g_pDx11Dev);
}

HRESULT InitAudioDevice()
{
    HRESULT hr = S_OK;
    IMMDeviceEnumerator *pIMMDevEnum = NULL;
    if (SUCCEEDED(hr))
    {
        hr = ::CoCreateInstance(
            __uuidof(MMDeviceEnumerator), NULL,
            CLSCTX_ALL, __uuidof(IMMDeviceEnumerator),
            (void**)&pIMMDevEnum);
    }
    if (SUCCEEDED(hr))
    {
        hr = pIMMDevEnum->GetDefaultAudioEndpoint(eRender, eConsole, &g_pIMMDev);
    }
    //LPWSTR pEndpointId = NULL;
    //if (SUCCEEDED(hr))
    //{
    //    hr = g_pIMMDev->GetId(&pEndpointId);
    //}
    //SafeRelease(pIMMDevEnum);

    //IMFAttributes *pAttributes = NULL;
    //IMFMediaSource *pSource = NULL;

    //hr = MFCreateAttributes(&pAttributes, 2);

    //// Set the device type to audio.
    //if (SUCCEEDED(hr))
    //{
    //    hr = pAttributes->SetGUID(
    //        MF_DEVSOURCE_ATTRIBUTE_SOURCE_TYPE,
    //        MF_DEVSOURCE_ATTRIBUTE_SOURCE_TYPE_AUDCAP_GUID
    //    );
    //}

    //// Set the endpoint ID.
    //if (SUCCEEDED(hr))
    //{
    //    hr = pAttributes->SetString(MF_DEVSOURCE_ATTRIBUTE_SOURCE_TYPE_AUDCAP_ENDPOINT_ID,pEndpointId);
    //}

    //if (SUCCEEDED(hr))
    //{
    //    hr = MFCreateDeviceSource(pAttributes, &pSource);
    //}

    //SafeRelease(pAttributes);

    if (SUCCEEDED(hr))
    {
        hr = g_pIMMDev->Activate(
            __uuidof(IAudioClient), CLSCTX_ALL,
            NULL, (void**)&g_pAudioClient);
    }
    if (SUCCEEDED(hr))
    {
        hr = g_pAudioClient->GetDevicePeriod(&g_hnsDefaultDevicePeriod, NULL);
    }
    if (SUCCEEDED(hr))
    {
        hr = g_pAudioClient->GetMixFormat(&g_pWaveFmt);
    }
    if (SUCCEEDED(hr))
    {
        hr = g_pAudioClient->Initialize(
            AUDCLNT_SHAREMODE_SHARED,
            //AUDCLNT_STREAMFLAGS_LOOPBACK | AUDCLNT_STREAMFLAGS_EVENTCALLBACK,
            AUDCLNT_STREAMFLAGS_LOOPBACK,
            0,
            0,
            g_pWaveFmt,
            NULL);
    }
    if (SUCCEEDED(hr))
    {
        hr = g_pAudioClient->GetBufferSize(&g_AudioBufFrameCount);
    }
    REFERENCE_TIME hnsActualDuration;
    if (SUCCEEDED(hr))
    {
        hnsActualDuration = REFTIMES_PER_SEC * g_AudioBufFrameCount / g_pWaveFmt->nSamplesPerSec;
    }
    if (SUCCEEDED(hr))
    {
        hr = g_pAudioClient->GetService(
            __uuidof(IAudioCaptureClient),
            (void**)&g_pAudioCaptureClient);
    }
    if (SUCCEEDED(hr))
    {
        hr = ((g_pAudioBuf = (PBYTE)malloc(g_MaxAudioBufSize)) != NULL) ? S_OK : E_FAIL;
        if (g_pAudioBuf) RtlZeroMemory(g_pAudioBuf, g_MaxAudioBufSize);
    }
    return hr;
}

void CleanupAudioDevice()
{
    if(g_pAudioClient) g_pAudioClient->Stop();
    SafeFree(g_pAudioBuf);
    SafeCoTaskMemFree(g_pWaveFmt);
    SafeRelease(g_pAudioCaptureClient);
    SafeRelease(g_pAudioClient);
    SafeRelease(g_pIMMDev);
}

HRESULT UpdateVideoInputTypeAttributes(IMFAttributes *pAttr)
{
    HRESULT hr = S_OK;
    if (!pAttr)
    {
        hr = E_INVALIDARG;
    }
    if (SUCCEEDED(hr))
    {
        hr = pAttr->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Video);
    }
    if (SUCCEEDED(hr))
    {
        hr = pAttr->SetGUID(MF_MT_SUBTYPE, DEFAULT_VIDEO_IN_FORMAT);
    }
    if (SUCCEEDED(hr))
    {
        hr = pAttr->SetUINT32(MF_MT_INTERLACE_MODE, MFVideoInterlace_Progressive);
    }
    if (SUCCEEDED(hr))
    {
        hr = ::MFSetAttributeSize(pAttr, MF_MT_FRAME_SIZE, g_videoWidth, g_videoHeight);
    }
    if (SUCCEEDED(hr))
    {
        hr = ::MFSetAttributeRatio(pAttr, MF_MT_FRAME_RATE, g_videoFps, 1);
    }
    if (SUCCEEDED(hr))
    {
        hr = ::MFSetAttributeRatio(pAttr, MF_MT_FRAME_RATE_RANGE_MIN, g_videoFps, 1);
    }
    if (SUCCEEDED(hr))
    {
        hr = ::MFSetAttributeRatio(pAttr, MF_MT_FRAME_RATE_RANGE_MAX, g_videoFps, 1);
    }
    if (SUCCEEDED(hr))
    {
        hr = ::MFSetAttributeRatio(pAttr, MF_MT_PIXEL_ASPECT_RATIO, 1, 1);
    }
    return hr;
}

HRESULT UpdateVideoOutputTypeAttributes(IMFAttributes *pAttr)
{
    HRESULT hr = S_OK;
    if (!pAttr)
    {
        hr = E_INVALIDARG;
    }
    if (SUCCEEDED(hr))
    {
        hr = pAttr->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Video);
    }
    if (SUCCEEDED(hr))
    {
        hr = pAttr->SetGUID(MF_MT_SUBTYPE, DEFAULT_VIDEO_OUT_FORMAT);
    }
    if (SUCCEEDED(hr))
    {
        hr = pAttr->SetUINT32(MF_MT_AVG_BITRATE, g_videoBitRate);
    }
    if (SUCCEEDED(hr))
    {
        hr = pAttr->SetUINT32(MF_MT_INTERLACE_MODE, MFVideoInterlace_Progressive);
    }
    if (SUCCEEDED(hr))
    {
        hr = ::MFSetAttributeSize(pAttr, MF_MT_FRAME_SIZE, g_videoWidth, g_videoHeight);
    }
    if (SUCCEEDED(hr))
    {
        hr = ::MFSetAttributeRatio(pAttr, MF_MT_FRAME_RATE, g_videoFps, 1);
    }
    if (SUCCEEDED(hr))
    {
        hr = ::MFSetAttributeRatio(pAttr, MF_MT_PIXEL_ASPECT_RATIO, 1, 1);
    }
    if (SUCCEEDED(hr))
    {
        hr = pAttr->SetUINT32(MF_MT_MPEG2_PROFILE, eAVEncH264VProfile_Main);
    }
    return hr;
}

HRESULT UpdateAudioOutputTypeAttributes(IMFAttributes *pAttr)
{
    HRESULT hr = S_OK;
    if (!pAttr)
    {
        hr = E_INVALIDARG;
    }
    if (SUCCEEDED(hr))
    {
        hr = pAttr->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Audio);
    }
    if (SUCCEEDED(hr))
    {
        hr = pAttr->SetGUID(MF_MT_SUBTYPE, DEFAULT_AUDIO_OUT_FORMAT);
    }
    if (SUCCEEDED(hr))
    {
        hr = pAttr->SetUINT32(MF_MT_COMPRESSED, 1);
    }
    if (SUCCEEDED(hr))
    {
        hr = pAttr->SetUINT32(MF_MT_AVG_BITRATE, DEFAULT_AUDIO_OUT_AVG_BITRATE);
    }
    if (SUCCEEDED(hr))
    {
        hr = pAttr->SetUINT32(MF_MT_AUDIO_BLOCK_ALIGNMENT, 1);
    }
    if (SUCCEEDED(hr))
    {
        hr = pAttr->SetUINT32(MF_MT_AUDIO_PREFER_WAVEFORMATEX, 1);
    }
    if (SUCCEEDED(hr))
    {
        hr = pAttr->SetGUID(MF_MT_AM_FORMAT_TYPE, FORMAT_WaveFormatEx);
    }
    if (SUCCEEDED(hr))
    {
        hr = pAttr->SetUINT32(MF_MT_AAC_AUDIO_PROFILE_LEVEL_INDICATION, DEFAULT_AUDIO_OUT_PROFILE_LEVEL);
    }
    if (SUCCEEDED(hr))
    {
        hr = pAttr->SetUINT32(MF_MT_FIXED_SIZE_SAMPLES, 0);
    }
    if (SUCCEEDED(hr))
    {
        hr = pAttr->SetUINT32(MF_MT_AAC_PAYLOAD_TYPE, 0);
    }
    if (SUCCEEDED(hr))
    {
        hr = pAttr->SetUINT32(MF_MT_AUDIO_NUM_CHANNELS, DEFAULT_AUDIO_OUT_NUM_CHANNELS);
    }
    if (SUCCEEDED(hr))
    {
        hr = pAttr->SetUINT32(MF_MT_AUDIO_SAMPLES_PER_SECOND, DEFAULT_AUDIO_OUT_SAMPLES_PER_SECOND);
    }
    if (SUCCEEDED(hr))
    {
        hr = pAttr->SetUINT32(MF_MT_AUDIO_AVG_BYTES_PER_SECOND, DEFAULT_AUDIO_OUT_AVG_BYTES_PER_SECOND);
    }
    if (SUCCEEDED(hr))
    {
        hr = pAttr->SetUINT32(MF_MT_AUDIO_BITS_PER_SAMPLE, DEFAULT_AUDIO_OUT_BITS_PER_SAMPLE);
    }
    if (SUCCEEDED(hr))
    {
        BYTE aacUserData[14] = { 0x00, 0x00, DEFAULT_AUDIO_OUT_PROFILE_LEVEL, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
        WORD AudioSpecificConfig = 0;
        AudioSpecificConfig |= 2 << 11;
        AudioSpecificConfig |= FindFreqIndex(g_pWaveFmt->nSamplesPerSec) << 7;
        AudioSpecificConfig |= g_pWaveFmt->nChannels << 3;
        aacUserData[12] = AudioSpecificConfig >> 8;
        aacUserData[13] = AudioSpecificConfig & 0x00FF;
        hr = pAttr->SetBlob(MF_MT_USER_DATA, (UINT8*)&(aacUserData), ARRAYSIZE(aacUserData));
    }
    return hr;
}

HRESULT PrepareVideoStream(IMFSinkWriter *pSinkWriter, DWORD *pVideoStreamIndex)
{
    HRESULT hr = S_OK;

    if (!pSinkWriter || !pVideoStreamIndex)
    {
        hr = E_FAIL;
    }
    *pVideoStreamIndex = 0;

    IMFMediaType *pMediaTypeOut = NULL;
    IMFMediaType *pMediaTypeIn = NULL;
    DWORD streamIndex = 0;

    // Set the output media type.
    if (SUCCEEDED(hr))
    {
        hr = ::MFCreateMediaType(&pMediaTypeOut);
    }
    if (SUCCEEDED(hr))
    {
        hr = UpdateVideoOutputTypeAttributes(pMediaTypeOut);
    }
    if (SUCCEEDED(hr))
    {
        hr = pSinkWriter->AddStream(pMediaTypeOut, &streamIndex);
    }

    // Set the input media type.
    if (SUCCEEDED(hr))
    {
        hr = ::MFCreateMediaType(&pMediaTypeIn);
    }
    if (SUCCEEDED(hr))
    {
        hr = UpdateVideoInputTypeAttributes(pMediaTypeIn);
    }
    if (SUCCEEDED(hr))
    {
        hr = pSinkWriter->SetInputMediaType(streamIndex, pMediaTypeIn, NULL);
    }

    if (SUCCEEDED(hr))
    {
        *pVideoStreamIndex = streamIndex;
    }

    SafeRelease(pMediaTypeOut);
    SafeRelease(pMediaTypeIn);
    return hr;
}

HRESULT PrepareVideoSinkSampleGrabber(SampleGrabberCB *pVideoSampleGrabber, IMFSinkWriter **ppVideoSinkWriter, IMFMediaSink **ppVideoSink, DWORD *pVideoStreamIndex)
{
    HRESULT hr = S_OK;

    if (!pVideoSampleGrabber)
    {
        hr = E_FAIL;
    }
    *ppVideoSinkWriter = NULL;
    *ppVideoSink = NULL;
    *pVideoStreamIndex = 0;

    IMFMediaType *pMediaTypeIn = NULL;
    IMFMediaType *pMediaTypeOut = NULL;
    IMFActivate *pVideoSinkActivate = NULL;
    IMFMediaSink *pVideoSink = NULL;
    IMFAttributes *pAttr = NULL;
    IMFSinkWriter *pVideoSinkWriter = NULL;
    DWORD streamIndex = 0;

    // Set the input media type.
    if (SUCCEEDED(hr))
    {
        hr = ::MFCreateMediaType(&pMediaTypeIn);
    }
    if (SUCCEEDED(hr))
    {
        hr = UpdateVideoInputTypeAttributes(pMediaTypeIn);
    }

    // Set the output media type.
    if (SUCCEEDED(hr))
    {
        hr = ::MFCreateMediaType(&pMediaTypeOut);
    }
    if (SUCCEEDED(hr))
    {
        hr = UpdateVideoOutputTypeAttributes(pMediaTypeOut);
    }

    // Create sink writer
    if (SUCCEEDED(hr))
    {
        hr = MFCreateSampleGrabberSinkActivate(pMediaTypeOut, pVideoSampleGrabber, &pVideoSinkActivate);
    }
    if (SUCCEEDED(hr))
    {
        hr = pVideoSinkActivate->SetUINT32(MF_SAMPLEGRABBERSINK_IGNORE_CLOCK, TRUE);
    }
    if (SUCCEEDED(hr))
    {
        hr = pVideoSinkActivate->ActivateObject(__uuidof(IMFMediaSink), (void**)&pVideoSink);
    }
    SafeRelease(pVideoSinkActivate);
    if (SUCCEEDED(hr))
    {
        hr = MFCreateAttributes(&pAttr, 0);
    }
    if (SUCCEEDED(hr))
    {
        hr = pAttr->SetUINT32(MF_LOW_LATENCY, USE_LOW_LATENCY_MODE);
        hr = pAttr->SetUINT32(MF_READWRITE_DISABLE_CONVERTERS, DISABLE_CONVERSION);
        hr = pAttr->SetUINT32(MF_READWRITE_ENABLE_HARDWARE_TRANSFORMS, USE_HW_MFT);
        hr = pAttr->SetGUID(MF_TRANSCODE_CONTAINERTYPE, DEFAULT_OUTPUT_CONTAINER_TYPE);
    }
    if (SUCCEEDED(hr))
    {
        hr = MFCreateSinkWriterFromMediaSink(pVideoSink, pAttr, &pVideoSinkWriter);
    }
    /*if (SUCCEEDED(hr))
    {
        hr = pVideoSinkWriter->AddStream(pMediaTypeOut, &streamIndex);
    }*/

    if (SUCCEEDED(hr))
    {
        hr = pVideoSinkWriter->SetInputMediaType(streamIndex, pMediaTypeIn, NULL);
    }

    if (SUCCEEDED(hr))
    {
        *ppVideoSinkWriter = pVideoSinkWriter;
        (*ppVideoSinkWriter)->AddRef();
        *ppVideoSink = pVideoSink;
        (*ppVideoSink)->AddRef();
        *pVideoStreamIndex = streamIndex;
    }

    SafeRelease(pVideoSinkWriter);
    SafeRelease(pAttr);
    SafeRelease(pVideoSink);
    SafeRelease(pVideoSinkActivate);
    SafeRelease(pMediaTypeOut);
    SafeRelease(pMediaTypeIn);
    return hr;
}

HRESULT PrepareAudioSinkSampleGrabber(SampleGrabberCB *pAudioSampleGrabber, IMFSinkWriter **ppAudioSinkWriter, IMFMediaSink **ppAudioSink, DWORD *pAudioStreamIndex)
{
    HRESULT hr = S_OK;

    if (!pAudioSampleGrabber)
    {
        hr = E_FAIL;
    }
    *ppAudioSinkWriter = NULL;
    *ppAudioSink = NULL;
    *pAudioStreamIndex = 0;

    IMFMediaType *pMediaTypeIn = NULL;
    IMFMediaType *pMediaTypeOut = NULL;
    IMFActivate *pAudioSinkActivate = NULL;
    IMFMediaSink *pAudioSink = NULL;
    IMFAttributes *pAttr = NULL;
    IMFSinkWriter *pAudioSinkWriter = NULL;
    DWORD streamIndex = 0;

    // Set the input media type.
    if (SUCCEEDED(hr))
    {
        hr = ::MFCreateMediaType(&pMediaTypeIn);
    }
    if (SUCCEEDED(hr))
    {
        hr = ::MFInitMediaTypeFromWaveFormatEx(pMediaTypeIn, g_pWaveFmt, sizeof(WAVEFORMATEX) + g_pWaveFmt->cbSize);
    }

    // Set the output media type.
    if (SUCCEEDED(hr))
    {
        hr = ::MFCreateMediaType(&pMediaTypeOut);
    }
    if (SUCCEEDED(hr))
    {
        hr = UpdateAudioOutputTypeAttributes(pMediaTypeOut);
    }

    // Create sink writer
    if (SUCCEEDED(hr))
    {
        hr = MFCreateSampleGrabberSinkActivate(pMediaTypeOut, pAudioSampleGrabber, &pAudioSinkActivate);
    }
    if (SUCCEEDED(hr))
    {
        hr = pAudioSinkActivate->SetUINT32(MF_SAMPLEGRABBERSINK_IGNORE_CLOCK, TRUE);
    }
    if (SUCCEEDED(hr))
    {
        hr = pAudioSinkActivate->ActivateObject(__uuidof(IMFMediaSink), (void**)&pAudioSink);
    }
    if (SUCCEEDED(hr))
    {
        hr = MFCreateAttributes(&pAttr, 0);
    }
    if (SUCCEEDED(hr))
    {
        hr = pAttr->SetUINT32(MF_LOW_LATENCY, USE_LOW_LATENCY_MODE);
        hr = pAttr->SetUINT32(MF_READWRITE_DISABLE_CONVERTERS, DISABLE_CONVERSION);
        hr = pAttr->SetUINT32(MF_READWRITE_ENABLE_HARDWARE_TRANSFORMS, USE_HW_MFT);
        hr = pAttr->SetGUID(MF_TRANSCODE_CONTAINERTYPE, DEFAULT_OUTPUT_CONTAINER_TYPE);
    }
    if (SUCCEEDED(hr))
    {
        hr = MFCreateSinkWriterFromMediaSink(pAudioSink, pAttr, &pAudioSinkWriter);
    }
    /*if (SUCCEEDED(hr))
    {
    hr = pVideoSinkWriter->AddStream(pMediaTypeOut, &streamIndex);
    }*/

    if (SUCCEEDED(hr))
    {
        hr = pAudioSinkWriter->SetInputMediaType(streamIndex, pMediaTypeIn, NULL);
    }

    if (SUCCEEDED(hr))
    {
        *ppAudioSinkWriter = pAudioSinkWriter;
        (*ppAudioSinkWriter)->AddRef();
        *ppAudioSink = pAudioSink;
        (*ppAudioSink)->AddRef();
        *pAudioStreamIndex = streamIndex;
    }

    SafeRelease(pAudioSinkWriter);
    SafeRelease(pAttr);
    SafeRelease(pAudioSink);
    SafeRelease(pAudioSinkActivate);
    SafeRelease(pMediaTypeOut);
    SafeRelease(pMediaTypeIn);
    return hr;
}

HRESULT PrepareAudioStream(IMFSinkWriter *pSinkWriter, DWORD *pAudioStreamIndex)
{
    HRESULT hr = S_OK;

    if (!pSinkWriter || !pAudioStreamIndex)
    {
        hr = E_FAIL;
    }
    *pAudioStreamIndex = 0;

    IMFMediaType *pMediaTypeOut = NULL;
    IMFMediaType *pMediaTypeIn = NULL;
    DWORD streamIndex = 0;

    // Set the output media type.
    if (SUCCEEDED(hr))
    {
        hr = ::MFCreateMediaType(&pMediaTypeOut);
    }
    if (SUCCEEDED(hr))
    {
        hr = UpdateAudioOutputTypeAttributes(pMediaTypeOut);
    }

    if (SUCCEEDED(hr))
    {
        hr = pSinkWriter->AddStream(pMediaTypeOut, &streamIndex);
    }

    // Set the input media type.
    if (SUCCEEDED(hr))
    {
        hr = ::MFCreateMediaType(&pMediaTypeIn);
    }
    if (SUCCEEDED(hr))
    {
        hr = ::MFInitMediaTypeFromWaveFormatEx(pMediaTypeIn, g_pWaveFmt, sizeof(WAVEFORMATEX) + g_pWaveFmt->cbSize);
    }
    if (SUCCEEDED(hr))
    {
        hr = pSinkWriter->SetInputMediaType(streamIndex, pMediaTypeIn, NULL);
    }

    if (SUCCEEDED(hr))
    {
        *pAudioStreamIndex = streamIndex;
    }

    SafeRelease(pMediaTypeOut);
    SafeRelease(pMediaTypeIn);
    return hr;
}

HRESULT InitRecorder()
{
    HRESULT hr = S_OK;

    SafeRelease(g_pMFVideoSinkWriter);
    SafeRelease(g_pMFAudioSinkWriter);
    SafeRelease(g_pMFAIOSinkWriter);
    g_videoStreamIndex = 0;
    g_audioStreamIndex = 0;

    if (SUCCEEDED(hr))
    {
        hr = MFStartup(MF_VERSION);
        Sleep(5);
    }

    IMFAttributes *pAttr = NULL;
    IMFSinkWriter *pSinkWriter = NULL;
    DWORD videoStreamIndex = 0;
    DWORD audioStreamIndex = 0;

    if (SUCCEEDED(hr))
    {
        hr = MFCreateAttributes(&pAttr, 0);
    }
    if (SUCCEEDED(hr))
    {
        hr = pAttr->SetUINT32(MF_LOW_LATENCY, USE_LOW_LATENCY_MODE);
        hr = pAttr->SetUINT32(MF_READWRITE_DISABLE_CONVERTERS, DISABLE_CONVERSION);
        hr = pAttr->SetUINT32(MF_READWRITE_ENABLE_HARDWARE_TRANSFORMS, USE_HW_MFT);
        hr = pAttr->SetGUID(MF_TRANSCODE_CONTAINERTYPE, DEFAULT_OUTPUT_CONTAINER_TYPE);
    }
    if (SUCCEEDED(hr))
    {
        if (SUCCEEDED(hr = ::MFCreateSinkWriterFromURL(g_bSplitAV ? g_VideoFileNameV.c_str() : g_VideoFileName.c_str(), NULL, pAttr, &pSinkWriter)))
        {
            (g_bSplitAV ? g_pMFVideoSinkWriter : g_pMFAIOSinkWriter) = pSinkWriter;
            (g_bSplitAV ? g_pMFVideoSinkWriter : g_pMFAIOSinkWriter)->AddRef();
        }
    }

    if (SUCCEEDED(hr))
    {
        if (SUCCEEDED(hr = PrepareVideoStream(pSinkWriter, &videoStreamIndex)))
        {
            g_videoStreamIndex = videoStreamIndex;
        }
    }

    g_fpAudioLog = NULL;
    if (g_bWithAudio && g_bSplitAV)
    {
        SafeRelease(pSinkWriter);
        if (SUCCEEDED(hr = ::MFCreateSinkWriterFromURL(g_VideoFileNameA.c_str(), NULL, pAttr, &pSinkWriter)))
        {
            g_pMFAudioSinkWriter = pSinkWriter;
            g_pMFAudioSinkWriter->AddRef();
            if (!_wfopen_s(&g_fpAudioLog, g_audiologFileName.c_str(), L"w+"))
            {
                fwrite("pts, duration, size\n", sizeof(CHAR), strlen("pts, duration, size\n"), g_fpAudioLog);
            }
        }
    }

    if (g_bWithAudio && SUCCEEDED(hr))
    {
        if (SUCCEEDED(hr = PrepareAudioStream(pSinkWriter, &audioStreamIndex)))
        {
            g_audioStreamIndex = audioStreamIndex;
        }
    }

    SafeRelease(pSinkWriter);
    if(pAttr)
    {
        hr = pAttr->DeleteAllItems();
    }
    SafeRelease(pAttr);

    return hr;
}

void FillAudioSample(IMFSinkWriter *pSinkWrite, REFERENCE_TIME pts, REFERENCE_TIME duration, PBYTE pSrcBuf, UINT32 bufSize)
{
    HRESULT hr = S_OK;
    IMFSample *pAudioSample = NULL;
    IMFMediaBuffer *pAudioMediaBuffer = NULL;
    BYTE *pAudioBuffer = NULL;

    if (SUCCEEDED(hr))
    {
        hr = ::MFCreateMemoryBuffer(bufSize, &pAudioMediaBuffer);
    }
    if (SUCCEEDED(hr))
    {
        hr = pAudioMediaBuffer->Lock(&pAudioBuffer, NULL, NULL);
    }
    if (SUCCEEDED(hr))
    {
        memcpy_s(pAudioBuffer, bufSize, pSrcBuf ? pSrcBuf : g_pAudioBuf, bufSize);
    }
    if (SUCCEEDED(hr))
    {
        hr = pAudioMediaBuffer->SetCurrentLength(bufSize);
    }
    if (pAudioBuffer)
    {
        hr = pAudioMediaBuffer->Unlock();
    }
    if (SUCCEEDED(hr))
    {
        hr = ::MFCreateSample(&pAudioSample);
    }
    /* if (!*pIsFirstFrame)
    {
    *pIsFirstFrame = TRUE;
    if (g_bLiveStreaming)
    {
    hr = g_pMFAudioSinkWriter->NotifyEndOfSegment(g_audioStreamIndex);
    }
    else
    {
    //hr = (g_bSplitAV ? g_pMFAudioSinkWriter : g_pMFAIOSinkWriter)->NotifyEndOfSegment(g_audioStreamIndex);
    }
    //hr = g_pMFSinkWriter->SendStreamTick(g_audioStreamIndex, (REFERENCE_TIME)((curMeasure.QuadPart - g_qpcAudioStart.QuadPart) * g_qpfPeroid * 10));
    IMFAttributes *pSampleAttr = NULL;
    if (SUCCEEDED(hr))
    {
    hr = pAudioSample->QueryInterface(__uuidof(IMFAttributes), (void **)&pSampleAttr);
    }
    if (SUCCEEDED(hr))
    {
    hr = pSampleAttr->SetUINT32(MFSampleExtension_Discontinuity, TRUE);
    }
    SafeRelease(pSampleAttr);
    } */
    if (SUCCEEDED(hr))
    {
        hr = pAudioSample->AddBuffer(pAudioMediaBuffer);
    }
    if (SUCCEEDED(hr))
    {
        hr = pAudioSample->SetSampleTime(pts);
    }
    if (SUCCEEDED(hr))
    {
        hr = pAudioSample->SetSampleDuration(duration);
    }
    if (SUCCEEDED(hr))
    {
        hr = pAudioSample->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Audio);
    }
    if (SUCCEEDED(hr))
    {
        hr = pSinkWrite->WriteSample(g_audioStreamIndex, pAudioSample);
    }
    SafeRelease(pAudioMediaBuffer);
    SafeRelease(pAudioSample);
}

void CleanupRecorder()
{
    HRESULT hr = S_OK;

    // Minimize A/V gap
    MF_SINK_WRITER_STATISTICS mfStats = { 0 };
    mfStats.cb = sizeof(mfStats);
    REFERENCE_TIME lastAPts = 0;
    REFERENCE_TIME lastVPts = 0;

    hr = ((g_bSplitAV || g_bLiveStreaming) ? g_pMFAudioSinkWriter : g_pMFAIOSinkWriter)->GetStatistics(g_audioStreamIndex, &mfStats);
    if (SUCCEEDED(hr))
    {
        lastAPts = mfStats.llLastTimestampReceived;
    }
    hr = ((g_bSplitAV || g_bLiveStreaming) ? g_pMFVideoSinkWriter : g_pMFAIOSinkWriter)->GetStatistics(g_videoStreamIndex, &mfStats);
    if (SUCCEEDED(hr))
    {
        lastVPts = mfStats.llLastTimestampReceived;
    }
    if (lastVPts > lastAPts)
    {
        REFERENCE_TIME currentFramesDuration = lastVPts - lastAPts;
        // Down round
        UINT32 numFakeFrames = (UINT32)(currentFramesDuration * g_pWaveFmt->nSamplesPerSec / REFTIMES_PER_SEC);
        // Adjust frame duration and pts
        currentFramesDuration = numFakeFrames * REFTIMES_PER_SEC / g_pWaveFmt->nSamplesPerSec;
        UINT32 bytesPerSample = g_pWaveFmt->nChannels * g_pWaveFmt->wBitsPerSample / 8;
        UINT32 currentSize = numFakeFrames * bytesPerSample;
        FillAudioSample(((g_bSplitAV || g_bLiveStreaming) ? g_pMFAudioSinkWriter : g_pMFAIOSinkWriter), lastAPts, currentFramesDuration, NULL, currentSize);

        g_audioPtsHelper.SaveLastPTS(lastAPts + currentFramesDuration);

        if (g_fpAudioLog)
        {
            CHAR buf[MAX_PATH];
            sprintf_s(buf, MAX_PATH, "%I64d, %I64d, %d\n", lastAPts, currentFramesDuration, currentSize);
            fwrite(buf, sizeof(CHAR), strlen(buf), g_fpAudioLog);
        }
    }

    if (g_pMFVideoSinkWriter)
    {
        hr = g_pMFVideoSinkWriter->Finalize();
    }
    if (g_pMFAudioSinkWriter)
    {
        hr = g_pMFAudioSinkWriter->Finalize();
    }
    if (g_pMFAIOSinkWriter)
    {
        hr = g_pMFAIOSinkWriter->Finalize();
    }

    if (g_fpAudioLog)
    {
        fclose(g_fpAudioLog);
    }

    SafeRelease(g_pMFAIOSinkWriter);
    SafeRelease(g_pMFVideoSinkWriter);
    SafeRelease(g_pMFAudioSinkWriter);
    g_videoStreamIndex = 0;
    g_audioStreamIndex = 0;
    ::MFShutdown();
}

DWORD WINAPI RTPTaskSchedulerThread(_In_ PVOID pParam)
{
    PRTP_TASK_THREAD_DATA pTData = (PRTP_TASK_THREAD_DATA)pParam;
    pTData->pEnv->taskScheduler().doEventLoop(&pTData->evStop);
    return 0;
}

void PostProcessVideoSample(void*)
{
}

void PostProcessAudioSample(void*)
{
}

unsigned char get_idx_sampling_freq()
{
    return FindFreqIndex(DEFAULT_AUDIO_OUT_SAMPLES_PER_SECOND);
}
void update_adts_fixed_header(p_adts_header p_adts_h)
{
    if (p_adts_h)
    {
        RtlZeroMemory(p_adts_h, sizeof(adts_header));
        p_adts_h->adts[0] = 0xFF;
        p_adts_h->adts[1] = 0xF0; // syncword : 12
        p_adts_h->adts[1] |= 1 << 3; // ID : 1, 
        p_adts_h->adts[1] |= 00 << 1; // layer : 2
        p_adts_h->adts[1] |= 1; //protection_absent : 1
        p_adts_h->adts[2] = 1 << 6; // profile : 2
        p_adts_h->adts[2] |= get_idx_sampling_freq() << 2; // sampling_frequency_index : 4
        p_adts_h->adts[2] |= 0 << 1; // private_bit : 1
        p_adts_h->adts[2] |= (DEFAULT_AUDIO_OUT_NUM_CHANNELS & 0x4) >> 2;
        p_adts_h->adts[3] = (DEFAULT_AUDIO_OUT_NUM_CHANNELS & 0x3) << 6; // channel_configuration : 3
        p_adts_h->adts[3] |= 0 << 5; // original_copy : 1
        p_adts_h->adts[3] |= 1 << 4; // home : 1
    }
}

void update_adts_variable_header(p_adts_header p_adts_h, DWORD size_sample)
{
    if (p_adts_h)
    {
        p_adts_h->adts[3] &= 0xF0;
        p_adts_h->adts[3] |= 0 << 3; // copyright_identification_bit : 1
        p_adts_h->adts[3] |= 0 << 2; // copyright_identification_start : 1
        DWORD frame_length = (size_sample + sizeof(((adts_header*)0)->adts)) & 0x1FFF; // only 13 bits
        p_adts_h->adts[3] |= (frame_length & 0x1800) >> 11;
        p_adts_h->adts[4] = (BYTE)((frame_length & 0x7F8) >> 3);
        p_adts_h->adts[5] = (frame_length & 0x7) << 5; // frame_length : 13
        DWORD adts_buffer_fullness = 0x7FF;
        p_adts_h->adts[5] |= (adts_buffer_fullness & 0x7C0) >> 6;
        p_adts_h->adts[6] = (adts_buffer_fullness & 0x3F) << 2; // adts_buffer_fullness : 11
        p_adts_h->adts[6] |= 0; // number_of_raw_data_blocks_in_frame : 2
    }
}

void CBProcessVideoSample(DWORD dwSampleFlags,
    LONGLONG llSampleTime, LONGLONG llSampleDuration, const BYTE * pSampleBuffer,
    DWORD dwSampleSize, IMFAttributes *pAttributes)
{
    /*if (!pRawFile)
    {
        SYSTEMTIME curTime = { 0 };
        GetLocalTime(&curTime);
        wchar_t path[MAX_PATH * 2];
        ZeroMemory(path, MAX_PATH * 2 * sizeof(wchar_t));
        GetCurrentDirectory(MAX_PATH * 2, path);
        wchar_t buf[MAX_PATH * 2];
        ZeroMemory(buf, MAX_PATH * 2 * sizeof(wchar_t));

        swprintf_s(buf, MAX_PATH * 2, L"%s\\%04d%02d%02d_%02d%02d%02d.264",
            path, curTime.wYear, curTime.wMonth, curTime.wDay, curTime.wHour, curTime.wMinute, curTime.wSecond);

        _wfopen_s(&pRawFile, buf, L"wb");
    }
        fwrite(pSampleBuffer, dwSampleSize, 1, pRawFile);
    */
    g_pVideoRB->feedFrame((PBYTE)pSampleBuffer, dwSampleSize, llSampleDuration);
}

void CBProcessAudioSample(DWORD dwSampleFlags,
    LONGLONG llSampleTime, LONGLONG llSampleDuration, const BYTE * pSampleBuffer,
    DWORD dwSampleSize, IMFAttributes *pAttributes)
{
    /*if (!pRawFile)
    {
        SYSTEMTIME curTime = { 0 };
        GetLocalTime(&curTime);
        wchar_t path[MAX_PATH * 2];
        ZeroMemory(path, MAX_PATH * 2 * sizeof(wchar_t));
        GetCurrentDirectory(MAX_PATH * 2, path);
        wchar_t buf[MAX_PATH * 2];
        ZeroMemory(buf, MAX_PATH * 2 * sizeof(wchar_t));

        swprintf_s(buf, MAX_PATH * 2, L"%s\\%04d%02d%02d_%02d%02d%02d.aac",
            path, curTime.wYear, curTime.wMonth, curTime.wDay, curTime.wHour, curTime.wMinute, curTime.wSecond);

        _wfopen_s(&pRawFile, buf, L"wb");
    }*/
    update_adts_variable_header(&adtsH, dwSampleSize);
    RtlZeroMemory(g_pAudioFrame, g_audioCacheSize);
    RtlCopyMemory(g_pAudioFrame, adtsH.adts, sizeof(adtsH.adts));
    RtlCopyMemory(g_pAudioFrame + sizeof(adtsH.adts), pSampleBuffer, dwSampleSize);
    g_pRTPAudioSource->feedFrame(g_pAudioFrame, sizeof(adtsH.adts) + dwSampleSize);
    /*fwrite(&adtsH, sizeof(((adts_header*)0)->adts), 1, pRawFile);
    fwrite(pSampleBuffer, dwSampleSize, 1, pRawFile);*/
}

FramedSource* source;
VOID CALLBACK DelayedTimerCB(PVOID pParam, BOOLEAN bTimerOrWaitFired)
{
    //source = ADTSAudioFileSource::createNew(*g_pRTPUsageEnv, "test.aac");
    //g_pRTPAudioSink->startPlaying(*source, PostProcessAudioSample, g_pRTPAudioSink);
    g_pRTPAudioSink->startPlaying(*g_pRTPAudioSource, PostProcessAudioSample, g_pRTPAudioSink);
    g_pRTPVideoSink->startPlaying(*g_pRTPVideoSource, PostProcessVideoSample, g_pRTPVideoSink);
}

HRESULT InitRTPSinkWriter()
{
    HRESULT hr = S_OK;

    g_videoCacheSize = (UINT32)((float)(CACHE_TIME_MS / MILLI_PER_SECOND * g_videoFps * g_videoWidth * g_videoHeight * RGB32_BPP)* COMPRESSION_RATIO);
    g_audioCacheSize = (UINT32)(CACHE_TIME_MS / MILLI_PER_SECOND * DEFAULT_AUDIO_OUT_AVG_BITRATE / 8);

    g_pVideoCache = (u_int8_t*)malloc(g_videoCacheSize);
    if (!g_pVideoCache)
    {
        hr = E_OUTOFMEMORY;
    }
    g_pAudioFrame = (PBYTE)malloc(g_audioCacheSize);
    if (!g_pAudioFrame)
    {
        hr = E_OUTOFMEMORY;
    }

    if (SUCCEEDED(hr))
    {
        hr = MFStartup(MF_VERSION);
        Sleep(5);
    }

    if (SUCCEEDED(hr))
    {
        hr = SampleGrabberCB::CreateInstance(&g_pVideoSampleGrabber);
    }
    if (SUCCEEDED(hr))
    {
        g_pVideoSampleGrabber->RegisterProcessSampleCallback(&CBProcessVideoSample);
    }
    if (SUCCEEDED(hr))
    {
        hr = SampleGrabberCB::CreateInstance(&g_pAudioSampleGrabber);
    }
    if (SUCCEEDED(hr))
    {
        g_pAudioSampleGrabber->RegisterProcessSampleCallback(&CBProcessAudioSample);
    }
    if (SUCCEEDED(hr))
    {
        hr = PrepareVideoSinkSampleGrabber(g_pVideoSampleGrabber, &g_pMFVideoSinkWriter, &g_pVideoSink, &g_videoStreamIndex);
    }
    if (g_bWithAudio && SUCCEEDED(hr))
    {
        hr = PrepareAudioSinkSampleGrabber(g_pAudioSampleGrabber, &g_pMFAudioSinkWriter, &g_pAudioSink, &g_audioStreamIndex);
    }

    g_RTPScheduler = BasicTaskScheduler::createNew();
    g_pRTPUsageEnv = BasicUsageEnvironment::createNew(*g_RTPScheduler);
    struct in_addr destinationAddress;
#ifdef USE_SSM
    destinationAddress.s_addr = chooseRandomIPv4SSMAddress(*g_pRTPUsageEnv);
#else
    destinationAddress.s_addr = our_inet_addr("192.168.1.15");// change to client IP address if not using SSM
#endif
    const unsigned short rtpPortNum = 6666;
    const unsigned char ttl = 1;
    g_pRTPVideoGroupsock = new Groupsock(*g_pRTPUsageEnv, destinationAddress, Port(rtpPortNum), ttl);
    g_pRTCPVideoGroupsock = new Groupsock(*g_pRTPUsageEnv, destinationAddress, Port(rtpPortNum + 1), ttl);
    g_pRTPAudioGroupsock = new Groupsock(*g_pRTPUsageEnv, destinationAddress, Port(rtpPortNum + 2), ttl);
    g_pRTCPAudioGroupsock = new Groupsock(*g_pRTPUsageEnv, destinationAddress, Port(rtpPortNum + 3), ttl);
#ifdef USE_SSM
    g_pRTPVideoGroupsock->multicastSendOnly();
    g_pRTCPVideoGroupsock->multicastSendOnly();
    g_pRTPAudioGroupsock->multicastSendOnly();
    g_pRTCPAudioGroupsock->multicastSendOnly();
#endif
    OutPacketBuffer::maxSize = 100000;
    g_pRTPVideoSink = H264VideoRTPSink::createNew(*g_pRTPUsageEnv, g_pRTPVideoGroupsock, 96);
    g_pRTPAudioSink = MPEG4GenericRTPSink::createNew(*g_pRTPUsageEnv, g_pRTPAudioGroupsock,
        96, DEFAULT_AUDIO_OUT_SAMPLES_PER_SECOND, "audio", "AAC-hbr", "1210", DEFAULT_AUDIO_OUT_NUM_CHANNELS);
    const unsigned estimatedVideoBandwidth = g_videoBitRate / 1024;
    const unsigned estimatedAudioBandwidth = DEFAULT_AUDIO_OUT_AVG_BITRATE / 1024;
    const unsigned maxCNAMElen = 100;
    unsigned char CNAME[maxCNAMElen + 1];
    gethostname((char*)CNAME, maxCNAMElen);
    CNAME[maxCNAMElen] = '\0'; // just in case
#ifdef USE_SSM
    Boolean const isSSM = True;
#else
    Boolean const isSSM = False;
#endif
    g_pRTCPVideo = RTCPInstance::createNew(*g_pRTPUsageEnv, g_pRTCPVideoGroupsock,
        estimatedVideoBandwidth, CNAME, g_pRTPVideoSink, NULL, isSSM);
    g_pRTCPAudio = RTCPInstance::createNew(*g_pRTPUsageEnv, g_pRTCPAudioGroupsock,
        estimatedAudioBandwidth, CNAME, g_pRTPAudioSink, NULL, isSSM);
    g_pRTSPServer = RTSPServer::createNew(*g_pRTPUsageEnv, 8554);

    CHAR url[MAX_PATH] = { 0 };
    size_t retVal = 0;
    _wcstombs_s_l(&retVal, url, MAX_PATH, g_VideoFileName.c_str(), _TRUNCATE, _get_current_locale());

    g_pRTSPServerMediaSession = ServerMediaSession::createNew(*g_pRTPUsageEnv, url, NULL, NULL);// , isSSM);
    g_pRTSPServerMediaSession->addSubsession(PassiveServerMediaSubsession::createNew(*g_pRTPVideoSink, g_pRTCPVideo));
    g_pRTSPServerMediaSession->addSubsession(PassiveServerMediaSubsession::createNew(*g_pRTPAudioSink, g_pRTCPAudio));
    g_pRTSPServer->addServerMediaSession(g_pRTSPServerMediaSession);

    char* cururl = g_pRTSPServer->rtspURL(g_pRTSPServerMediaSession);
    *g_pRTPUsageEnv << url << "\"\n";
    delete[] cururl;

    g_RTPTData.pEnv = g_pRTPUsageEnv;
    g_RTPTData.evStop = 0;
    g_RTPTData.hThread = ::CreateThread(nullptr, 0, RTPTaskSchedulerThread, &g_RTPTData, 0, &g_RTPTData.tId);

    g_pVideoRB = ByteStreamRingBufferSource::createNew(*g_pRTPUsageEnv, g_pVideoCache, (u_int64_t)g_videoCacheSize);
    FramedSource* videoES = g_pVideoRB;
    g_pRTPVideoSource = H264VideoStreamFramer::createNew(*g_pRTPUsageEnv, videoES);

    update_adts_fixed_header(&adtsH);
    g_pRTPAudioSource = ADTSAudioRingBufferSource::createNew(*g_pRTPUsageEnv, 
        g_audioCacheSize, 
        DEFAULT_AUDIO_OUT_AVG_BITRATE, 
        (adtsH.adts[2]&0xC0) >> 6,
        get_idx_sampling_freq(), 
        (UINT8)DEFAULT_AUDIO_OUT_NUM_CHANNELS);

    g_hTimerQueue = CreateTimerQueue();
    if (g_hTimerQueue)
    {
        if (!CreateTimerQueueTimer(&g_hTimer, g_hTimerQueue,
            (WAITORTIMERCALLBACK)DelayedTimerCB, NULL, DELAY_START_TIME_MS, 0, WT_EXECUTEDEFAULT))
        {
            hr = E_FAIL;
        }
    }

    return hr;
}

void CleanupRTPSink()
{
    HRESULT hr = S_OK;

    g_pRTPVideoSink->stopPlaying();
    g_pRTPAudioSink->stopPlaying();

    g_RTPTData.evStop = 1;
    ::WaitForSingleObjectEx(g_RTPTData.hThread, 3000, FALSE);

    //g_pRTSPServerMediaSession->close(g_pRTSPServerMediaSession);
    g_pRTSPServer->deleteServerMediaSession(g_pRTSPServerMediaSession);
    g_pRTSPServer->close(g_pRTSPServer);
    g_pRTCPVideo->close(g_pRTCPVideo);
    g_pRTCPAudio->close(g_pRTCPAudio);
    g_pRTPVideoSink->close(g_pRTPVideoSink);
    g_pRTPAudioSink->close(g_pRTPAudioSink);
    g_pRTPVideoGroupsock->removeAllDestinations();
    g_pRTCPVideoGroupsock->removeAllDestinations();
    g_pRTPAudioGroupsock->removeAllDestinations();
    g_pRTCPAudioGroupsock->removeAllDestinations();
    Medium::close(g_pRTPVideoSource);
    Medium::close(g_pRTPAudioSource);
    g_pRTPUsageEnv->reclaim();
    SafeDelete(g_RTPScheduler);

    if (g_pMFVideoSinkWriter)
    {
        hr = g_pMFVideoSinkWriter->Finalize();
    }
    SafeRelease(g_pMFVideoSinkWriter);
    if (g_pMFAudioSinkWriter)
    {
        hr = g_pMFAudioSinkWriter->Finalize();
    }
    SafeRelease(g_pMFAudioSinkWriter);
    g_pVideoSink->Shutdown();
    g_pAudioSink->Shutdown();
    SafeRelease(g_pVideoSink);
    SafeRelease(g_pAudioSink);
    SafeRelease(g_pVideoSampleGrabber);
    SafeRelease(g_pAudioSampleGrabber);
    g_videoStreamIndex = 0;
    g_audioStreamIndex = 0;
    ::MFShutdown();
    SafeFree(g_pVideoCache);
    SafeFree(g_pAudioFrame);
    DeleteTimerQueueTimer(g_hTimerQueue, g_hTimer, INVALID_HANDLE_VALUE);
    DeleteTimerQueue(g_hTimerQueue);

    if (pRawFile)
    {
        fclose(pRawFile);
        pRawFile = NULL;
    }
}

UINT32 g_frameCount = 0;
void DoRecordVideoFrame(BOOL bDoRecord, BOOL bFirstFrame)
{
    HRESULT hr = S_OK;
    if (!bDoRecord)
    {
        return;
    }

    g_pDx11DevCtx->CopyResource(g_pVideoInputTex, g_pParentSharedRTTex);

    D3D11_VIDEO_PROCESSOR_STREAM stream = { TRUE, 0, 0, 0, 0, NULL, g_pDx11VideoInputView, NULL };
    hr = g_pDx11VideoDevCtx->VideoProcessorBlt(g_pDx11VideoProcessor, g_pDx11VideoOutputView, 0, 1, &stream);

    // Record
    IMFSample *pSample = NULL;
    IMFMediaBuffer *pBuffer = NULL;
    DWORD mediaBufferLength = 0;
    IMF2DBuffer *p2DBuffer = NULL;
    if (SUCCEEDED(hr))
    {
        hr = ::MFCreateDXGISurfaceBuffer(__uuidof(ID3D11Texture2D), g_pVideoOutputTex, 0, false, &pBuffer);
    }
    if (SUCCEEDED(hr))
    {
        hr = pBuffer->QueryInterface(__uuidof(IMF2DBuffer), (void**)&p2DBuffer);
    }
    if (SUCCEEDED(hr))
    {
        hr = p2DBuffer->GetContiguousLength(&mediaBufferLength);
    }

    // Set the data length of the buffer.
    if (SUCCEEDED(hr))
    {
        hr = pBuffer->SetCurrentLength(mediaBufferLength);
    }

    // Create a media sample and add the buffer to the sample.
    if (SUCCEEDED(hr))
    {
        hr = ::MFCreateSample(&pSample);
    }
    if (SUCCEEDED(hr))
    {
        hr = pSample->AddBuffer(pBuffer);
    }

    // Set the time stamp and the duration.
    LARGE_INTEGER curMeasure = { 0 };
    ::QueryPerformanceCounter(&curMeasure);
    REFERENCE_TIME videoDurationHns = REFTIMES_PER_SEC / g_videoFps;

    if(bFirstFrame)
    {
        hr = ((g_bSplitAV || g_bLiveStreaming) ? g_pMFVideoSinkWriter : g_pMFAIOSinkWriter)->SendStreamTick(g_videoStreamIndex, g_ptsVideo);
        g_ptsVideo += videoDurationHns;
        hr = pSample->SetUINT32(MFSampleExtension_Discontinuity, TRUE);
    }

    if (SUCCEEDED(hr))
    {
        hr = pSample->SetSampleTime(g_ptsVideo);
        //hr = pSample->SetSampleTime(pts);
    }
    if (SUCCEEDED(hr))
    {
        hr = pSample->SetSampleDuration(videoDurationHns);
    }
    if (SUCCEEDED(hr))
    {
        hr = UpdateVideoOutputTypeAttributes(pSample);
        //if (bFirstFrame || 
        //    (g_frameCount % (CACHE_TIME_MS / MILLI_PER_SECOND * g_videoFps) == 0)) // In case live stream, force I-frame periodically
        //{
        //    if (g_bLiveStreaming)
        //    {
        //        hr = g_pMFVideoSinkWriter->SendStreamTick(g_videoStreamIndex, g_ptsVideo);
        //    }
        //    else
        //    {
        //        //hr = g_pMFAIOSinkWriter->SendStreamTick(g_videoStreamIndex, g_ptsVideo);
        //    }
        //    //hr = pSample->SetUINT32(MFSampleExtension_Discontinuity, TRUE);
        //    g_frameCount = 0;// Set I-frame every x frames, only for live streaming
        //}
    }

    if (SUCCEEDED(hr))
    {
        hr = pSample->SetUINT32(MFSampleExtension_CleanPoint, TRUE);
    }

    g_ptsVideo += videoDurationHns;
    RtlCopyMemory(&g_qpcPrevVideo, &curMeasure, sizeof(LARGE_INTEGER));

    // Send the sample to the Sink Writer.
    if (SUCCEEDED(hr) && bDoRecord)
    {
        hr = ((g_bSplitAV || g_bLiveStreaming) ? g_pMFVideoSinkWriter : g_pMFAIOSinkWriter)->WriteSample(g_videoStreamIndex, pSample);
    }

    SafeRelease(pSample);
    SafeRelease(p2DBuffer);
    SafeRelease(pBuffer);
}

void DoRecordAudioFrame(PBOOL pIsFirstFrame, LONG period, BOOL bKeepSample)
{
    HRESULT hr = S_OK;

    IMFSample *pAudioSample = NULL;
    IMFMediaBuffer *pAudioMediaBuffer = NULL;
    BYTE *pAudioBuffer = NULL;

    UINT32 bytesPerSample = g_pWaveFmt->nChannels * g_pWaveFmt->wBitsPerSample / 8;
    UINT32 currentSize = bytesPerSample;

    UINT32 numFrames = 0;
    hr = g_pAudioCaptureClient->GetNextPacketSize(&numFrames);
    if (SUCCEEDED(hr))
    {
        if (numFrames)
        {
            UINT32 numFramesRead = 0;
            REFERENCE_TIME pts = 0;
            REFERENCE_TIME currentFramesDuration = 0;
            BYTE *pData = NULL;
            DWORD dwFlags = 0;
            UINT64 u64DevicePosition = 0;
            UINT64 u64QPCPosition = 0;
            do 
            {
                hr = g_pAudioCaptureClient->GetBuffer(&pData, &numFramesRead, &dwFlags, &u64DevicePosition, &u64QPCPosition);
                currentSize = numFramesRead * bytesPerSample;
                currentFramesDuration = numFramesRead * REFTIMES_PER_SEC / g_pWaveFmt->nSamplesPerSec;

                if (SUCCEEDED(hr))
                {
                    if (bKeepSample)
                    {
                        g_audioPtsHelper.GetLastPTS(pts);

                        FillAudioSample(((g_bSplitAV || g_bLiveStreaming) ? g_pMFAudioSinkWriter : g_pMFAIOSinkWriter), pts, currentFramesDuration, g_bMute ? g_pAudioBuf : pData, currentSize);

                        g_audioPtsHelper.SaveLastPTS(pts + currentFramesDuration);

                        if (g_fpAudioLog)
                        {
                            CHAR buf[MAX_PATH];
                            sprintf_s(buf, MAX_PATH, "%I64d, %I64d, %d\n", pts, currentFramesDuration, currentSize);
                            fwrite(buf, sizeof(CHAR), strlen(buf), g_fpAudioLog);
                        }
                    }
                    hr = g_pAudioCaptureClient->ReleaseBuffer(numFramesRead);
                }
                hr = g_pAudioCaptureClient->GetNextPacketSize(&numFrames);

            } while (SUCCEEDED(hr) && numFrames);
        }
        else
        {
            REFERENCE_TIME pts = 0;
            REFERENCE_TIME ptsNonStop = 0;
            REFERENCE_TIME lastPts = 0;
            g_audioPtsHelper.GetPTS(pts, ptsNonStop);
            g_audioPtsHelper.GetLastPTS(lastPts);
            if(pts > lastPts)
            {
                REFERENCE_TIME currentFramesDuration = pts - lastPts;
                // Down round
                UINT32 numFakeFrames = (UINT32)(currentFramesDuration * g_pWaveFmt->nSamplesPerSec / REFTIMES_PER_SEC);
                // Adjust frame duration and pts
                currentFramesDuration = numFakeFrames * REFTIMES_PER_SEC / g_pWaveFmt->nSamplesPerSec;
                currentSize = numFakeFrames * bytesPerSample;
                if (bKeepSample)
                {
                    FillAudioSample(((g_bSplitAV || g_bLiveStreaming) ? g_pMFAudioSinkWriter : g_pMFAIOSinkWriter), lastPts, currentFramesDuration, NULL, currentSize);

                    g_audioPtsHelper.SaveLastPTS(lastPts + currentFramesDuration);

                    if (g_fpAudioLog)
                    {
                        CHAR buf[MAX_PATH];
                        sprintf_s(buf, MAX_PATH, "%I64d, %I64d, %d\n", pts, currentFramesDuration, currentSize);
                        fwrite(buf, sizeof(CHAR), strlen(buf), g_fpAudioLog);
                    }
                }
            }
        }
    }
}

DWORD WINAPI RecordVideoWorkThread(_In_ PVOID pParam)
{
    HRESULT hr = S_OK;
    PTHREAD_DATA pTData = (PTHREAD_DATA)pParam;

    BOOL comInit = FALSE;
    if (SUCCEEDED(hr))
    {
        hr = ::CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
        if (SUCCEEDED(hr))
        {
            comInit = TRUE;
        }
    }
    if (SUCCEEDED(hr))
    {
        hr = InitDx11Device();
    }
    if (SUCCEEDED(hr))
    {
        if (g_bLiveStreaming)
        {
            hr = InitRTPSinkWriter();
        }
        else
        {
            hr = InitRecorder();
        }
    }
    DWORD nTaskIndex = 0;
    if (SUCCEEDED(hr))
    {
        hr = ((g_hVideoThreadTask = ::AvSetMmThreadCharacteristics(L"Games", &nTaskIndex)) != NULL) ? S_OK : E_FAIL;
    }

    HANDLE hWakeUp = NULL;
    if (SUCCEEDED(hr))
    {
        hr = (hWakeUp = ::CreateWaitableTimer(NULL, FALSE, NULL)) ? S_OK : E_FAIL;
    }
    LARGE_INTEGER dueTimeHns = { 0 };
    dueTimeHns.QuadPart = -(REFTIMES_PER_SEC / g_videoFps);
    LONG peroidMils = (LONG)REFTIMES_PER_SEC / g_videoFps / (10 * 1000);
    if (SUCCEEDED(hr))
    {
        if (!::SetWaitableTimer(hWakeUp, &dueTimeHns, peroidMils, NULL, NULL, FALSE))
        {
            hr = E_FAIL;
        }
    }

    BOOL recordStart = FALSE;
    BOOL bDoRecord = FALSE;
    BOOL firstFrame = FALSE;
    ::SetEvent(g_evVideoReady);
    while (SUCCEEDED(hr) && (::WaitForSingleObjectEx(g_evWorkThreadExit, 0, FALSE) == WAIT_TIMEOUT))
    {
        if (::WaitForSingleObjectEx(hWakeUp, 1, FALSE) == WAIT_OBJECT_0)
        {
            if (!recordStart)
            {
                if (g_bLiveStreaming)
                {
                    if (g_pMFVideoSinkWriter)
                    {
                        hr = g_pMFVideoSinkWriter->BeginWriting();
                    }
                    if (g_bWithAudio && g_pMFAudioSinkWriter)
                    {
                        hr = g_pMFAudioSinkWriter->BeginWriting();
                    }
                }
                else
                {
                    if (g_bSplitAV)
                    {
                        if (g_pMFVideoSinkWriter)
                        {
                            hr = g_pMFVideoSinkWriter->BeginWriting();
                        }
                        if (g_bWithAudio && g_pMFAudioSinkWriter)
                        {
                            hr = g_pMFAudioSinkWriter->BeginWriting();
                        }
                    }
                    else
                    {
                        if (g_pMFAIOSinkWriter)
                        {
                            hr = g_pMFAIOSinkWriter->BeginWriting();
                        }
                    }
                }
                ::SetEvent(g_evRecordStart);
                ::EnterCriticalSection(&g_csDelayPause);
                ::SetEvent(g_evRecordActive);
                ::LeaveCriticalSection(&g_csDelayPause);
                g_audioPtsHelper.ReStartPTS();
                recordStart = TRUE;
                firstFrame = TRUE;
                g_ptsVideo = 0;
            }
            if (::WaitForSingleObjectEx(g_evRecordActive, 0, FALSE) == WAIT_OBJECT_0)
            {
                if (!bDoRecord)
                {
                    firstFrame = TRUE;
                }
                else
                {
                    firstFrame = FALSE;
                }
                bDoRecord = TRUE;
            }
            else
            {
                bDoRecord = FALSE;
            }
            DoRecordVideoFrame(bDoRecord, firstFrame);
        }
    }
    ::WaitForSingleObjectEx(g_hRecordAudioWorkThread, INFINITE, FALSE);

    if(g_hVideoThreadTask) ::AvRevertMmThreadCharacteristics(g_hVideoThreadTask);
    if (g_bLiveStreaming)
    {
        CleanupRTPSink();
    }
    else
    {
        CleanupRecorder();
    }
    CleanupAudioDevice();
    CleanupDx11Device();
    if (comInit) ::CoUninitialize();
    return 0;
}

DWORD WINAPI RecordAudioWorkThread(_In_ PVOID pParam)
{
    HRESULT hr = S_OK;

    BOOL comInit = FALSE;
    if (SUCCEEDED(hr))
    {
        hr = ::CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
        if (SUCCEEDED(hr))
        {
            comInit = TRUE;
        }
    }
    if (SUCCEEDED(hr))
    {
        hr = InitAudioDevice();
    }
    DWORD nTaskIndex = 0;
    if (SUCCEEDED(hr))
    {
        hr = ((g_hAudioThreadTask = ::AvSetMmThreadCharacteristics(L"Audio", &nTaskIndex)) != NULL) ? S_OK : E_FAIL;
    }
    HANDLE hWakeUp = NULL;
    if (SUCCEEDED(hr))
    {
        hr = (hWakeUp = ::CreateWaitableTimer(NULL, FALSE, NULL)) ? S_OK : E_FAIL;
    }
    LARGE_INTEGER dueTime = {0};
    dueTime.QuadPart = -(g_hnsDefaultDevicePeriod / 2);
    LONG peroidMils = (LONG)g_hnsDefaultDevicePeriod / 2 / (10 * 1000);
    if (SUCCEEDED(hr))
    {
        if (!::SetWaitableTimer(hWakeUp, &dueTime, peroidMils, NULL, NULL, FALSE))
        {
            hr = E_FAIL;
        }
    }
    BOOL recordStart = FALSE;
    BOOL bFirstFrame = FALSE;
    ::SetEvent(g_evAudioReady);
    while (SUCCEEDED(hr) && (::WaitForSingleObjectEx(g_evWorkThreadExit, 0, FALSE) == WAIT_TIMEOUT))
    {
        if (::WaitForSingleObjectEx(g_evRecordStart, 0, FALSE) == WAIT_OBJECT_0)
        {
            if (!recordStart && g_pAudioClient)
            {
                g_pAudioClient->Start();
                recordStart = TRUE;
                bFirstFrame = TRUE;
                ::QueryPerformanceCounter(&g_qpcAudioStart);
                g_ptsAudioNext = 0;
            }
            DoRecordAudioFrame(&bFirstFrame, peroidMils, (::WaitForSingleObjectEx(g_evRecordActive, 0, FALSE) == WAIT_OBJECT_0));
        }
        ::WaitForSingleObjectEx(hWakeUp, peroidMils * 2, FALSE);
    }
    ::CancelWaitableTimer(hWakeUp);
    SafeCloseHandle(hWakeUp);
    if (g_hAudioThreadTask) ::AvRevertMmThreadCharacteristics(g_hAudioThreadTask);
    if (comInit) ::CoUninitialize();
    return 0;
}

RECORDER_INTERFACE_API BOOL RecorderInterface::StartRecording(BOOL bStreaming, HANDLE hRT, PWCHAR pFileName, UINT32 width, UINT32 height, UINT32 bitRate, UINT32 fps)
{
    BOOL isSuccess = FALSE;
    HRESULT hr = S_OK;

    g_bMute = FALSE;

    if (g_Status != RECORD_STATUS_STOPPED)
        return isSuccess;

    LARGE_INTEGER qpfFreq = { 0 };
    ::QueryPerformanceFrequency(&qpfFreq);
    g_qpfPeroid = 1 / ((double)qpfFreq.QuadPart);
    g_qpfPeroid *= 1000000.0;

    g_hRT = hRT;
    g_VideoFileName = std::wstring(pFileName);
    g_VideoFileNameV = std::wstring(pFileName, 0, PathFindExtension(pFileName) - pFileName) + std::wstring(L"_V") + std::wstring(PathFindExtension(pFileName));
    g_VideoFileNameA = std::wstring(pFileName, 0, PathFindExtension(pFileName) - pFileName) + std::wstring(L"_A") + std::wstring(PathFindExtension(pFileName));
    g_audiologFileName = std::wstring(pFileName, 0, PathFindExtension(pFileName) - pFileName) + std::wstring(L"_A.log");
    g_videoWidth = width;
    g_videoHeight = height;
    g_videoBitRate = bitRate;
    // Scale then round up to nearest x10 integer
    g_videoFps = (UINT32)((FLOAT)fps * g_fpsRatio + 0.5) + (10 - (UINT32)((FLOAT)fps * g_fpsRatio + 0.5) % 10);

    g_evAudioReady = ::CreateEvent(NULL, TRUE, FALSE, NULL);
    g_evVideoReady = ::CreateEvent(NULL, TRUE, FALSE, NULL);
    g_evWorkThreadExit = ::CreateEvent(NULL, TRUE, FALSE, NULL);
    g_evFrameUpdate = ::CreateEvent(NULL, TRUE, FALSE, NULL);
    g_evRecordStart = ::CreateEvent(NULL, TRUE, FALSE, NULL);
    g_evRecordActive = ::CreateEvent(NULL, TRUE, FALSE, NULL);
    ::InitializeCriticalSection(&g_csDelayPause);
    ::ResetEvent(g_evAudioReady);
    ::ResetEvent(g_evVideoReady);
    ::ResetEvent(g_evWorkThreadExit);
    ::ResetEvent(g_evFrameUpdate);
    ::ResetEvent(g_evRecordStart);
    ::ResetEvent(g_evRecordActive);

    g_bLiveStreaming = bStreaming;

    DWORD tID = 0;
    if (g_bWithAudio)
    {
        g_hRecordAudioWorkThread = ::CreateThread(nullptr, 0, RecordAudioWorkThread, NULL, 0, &tID);
        ::WaitForSingleObjectEx(g_evAudioReady, INFINITE, FALSE);
    }
    g_hRecordVideoWorkThread = ::CreateThread(nullptr, 0, RecordVideoWorkThread, NULL, 0, &tID);
    ::WaitForSingleObjectEx(g_evVideoReady, INFINITE, FALSE);
    if (!g_hRecordVideoWorkThread || (g_bWithAudio && !g_hRecordAudioWorkThread))
    {
        hr = E_FAIL;
    }

    if (SUCCEEDED(hr))
    {
        isSuccess = TRUE;
        g_Status = RECORD_STATUS_STARTED;
    }

    return isSuccess;
}

RECORDER_INTERFACE_API VOID RecorderInterface::PauseRecording()
{
    if (g_bLiveStreaming)
    {
        return;
    }
    if (g_Status == RECORD_STATUS_STARTED)
    {
        ::EnterCriticalSection(&g_csDelayPause);
        ::ResetEvent(g_evRecordActive);
        g_audioPtsHelper.PausePTS();
        g_Status = RECORD_STATUS_PAUSED;
        ::LeaveCriticalSection(&g_csDelayPause);
    }
}

RECORDER_INTERFACE_API VOID RecorderInterface::ResumeRecording()
{
    if (g_bLiveStreaming)
    {
        return;
    }
    if (g_Status == RECORD_STATUS_PAUSED)
    {
        ::EnterCriticalSection(&g_csDelayPause);
        g_audioPtsHelper.ResumePTS();
        ::SetEvent(g_evRecordActive);
        g_Status = RECORD_STATUS_STARTED;
        ::LeaveCriticalSection(&g_csDelayPause);
    }
}

RECORDER_INTERFACE_API VOID RecorderInterface::NotifyFrameUpdate()
{
    if (::WaitForSingleObjectEx(g_evVideoReady, 0, FALSE) == WAIT_OBJECT_0)
    {
        ::SetEvent(g_evFrameUpdate);
    }
}

RECORDER_INTERFACE_API BOOL RecorderInterface::StopRecording()
{
    BOOL isSuccess = FALSE;
    ::SetEvent(g_evWorkThreadExit);
    ::SetEvent(g_evFrameUpdate);
    ::WaitForSingleObjectEx(g_hRecordVideoWorkThread, 3000, FALSE);
    SafeCloseHandle(g_hRecordAudioWorkThread);
    ::DeleteCriticalSection(&g_csDelayPause);
    SafeCloseHandle(g_evRecordActive);
    SafeCloseHandle(g_evRecordStart);
    SafeCloseHandle(g_evFrameUpdate);
    SafeCloseHandle(g_evWorkThreadExit);
    SafeCloseHandle(g_evVideoReady);
    SafeCloseHandle(g_evAudioReady);
    g_Status = RECORD_STATUS_STOPPED;
    return isSuccess;
}

RecorderInterface::RECORD_STATUS RecorderInterface::GetRecordStatus()
{
    return g_Status;
}

VOID RecorderInterface::SetAudioMute(BOOL bMute)
{
    g_bMute = bMute;
}

BOOL RecorderInterface::GetAudioMuteStatus()
{
    return g_bMute;
}
