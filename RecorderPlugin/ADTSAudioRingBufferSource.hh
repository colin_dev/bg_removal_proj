/**********
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation; either version 3 of the License, or (at your
option) any later version. (See <http://www.gnu.org/copyleft/lesser.html>.)

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
**********/
// "liveMedia"
// Copyright (c) 1996-2018 Live Networks, Inc.  All rights reserved.
// A source object for AAC audio files in ADTS format
// C++ header

#ifndef _ADTS_AUDIO_RING_BUFFER_SOURCE_HH
#define _ADTS_AUDIO_RING_BUFFER_SOURCE_HH

#ifndef _FRAMED_SOURCE_HH
#include "FramedSource.hh"
#endif

#include <windows.h>
#include <stdlib.h> 

class ADTSAudioRingBufferSource: public FramedSource {
public:
  static ADTSAudioRingBufferSource* createNew(UsageEnvironment& env, 
      UINT32 maxBufferSize,
      UINT32 avg_bps,
      UINT32 profile,
      UINT32 sampling_frequency_index,
      UINT32 channel_configuration);

  unsigned samplingFrequency() const { return fSamplingFrequency; }
  unsigned numChannels() const { return fNumChannels; }
  char const* configStr() const { return fConfigStr; }
  void feedFrame(PBYTE pCurFrame, unsigned frameSize);
      // returns the 'AudioSpecificConfig' for this stream (in ASCII form)

private:
    ADTSAudioRingBufferSource(UsageEnvironment& env, UINT32 maxBufferSize, UINT32 avg_bps, UINT32 profile,
        UINT32 samplingFrequencyIndex, UINT32 channelConfiguration);
	// called only by createNew()

  virtual ~ADTSAudioRingBufferSource();

private:
  // redefined virtual functions:
  virtual void doGetNextFrame();

private:
    PBYTE pBuffer;
    UINT32 fBufferSize;
    // RB PTR
    UINT32 wptr;
    UINT32 rptr;
    UINT32 eor;//end of ring
  CRITICAL_SECTION cs;
  unsigned fSamplingFrequency;
  unsigned fNumChannels;
  unsigned fuSecsPerFrame;
  char fConfigStr[5];
};

#endif
