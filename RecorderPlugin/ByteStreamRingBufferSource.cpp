/**********
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation; either version 3 of the License, or (at your
option) any later version. (See <http://www.gnu.org/copyleft/lesser.html>.)

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
**********/
// "liveMedia"
// Copyright (c) 1996-2018 Live Networks, Inc.  All rights reserved.
// A class for streaming data from a (static) memory buffer, as if it were a file.
// Implementation

#include "stdafx.h"
#include "ByteStreamRingBufferSource.h"
#include "GroupsockHelper.hh"
#include <assert.h>

////////// ByteStreamRingBufferSource //////////

ByteStreamRingBufferSource*
ByteStreamRingBufferSource::createNew(UsageEnvironment& env,
    u_int8_t* buffer, unsigned bufferSize,
    unsigned preferredFrameSize,
    unsigned playTimePerFrame) {
    if (buffer == NULL) return NULL;

    return new ByteStreamRingBufferSource(env, buffer, bufferSize, preferredFrameSize, playTimePerFrame);
}

ByteStreamRingBufferSource::ByteStreamRingBufferSource(UsageEnvironment& env,
    u_int8_t* buffer, unsigned bufferSize,
    unsigned preferredFrameSize,
    unsigned playTimePerFrame)
    : FramedSource(env), fBuffer(buffer), fBufferSize(bufferSize), fCurIndex(0), 
    fPreferredFrameSize(preferredFrameSize), fPlayTimePerFrame(playTimePerFrame), fLastPlayTime(0),
    fLimitNumBytesToStream(False), fNumBytesToStream(0), llLatestSampleDurationus(0), wptr(0), rptr(0), eor(bufferSize){
}

ByteStreamRingBufferSource::~ByteStreamRingBufferSource() {
}

void ByteStreamRingBufferSource::seekToByteAbsolute(u_int64_t byteNumber, u_int64_t numBytesToStream) {
    fCurIndex = byteNumber;
    if (fCurIndex > fBufferSize) fCurIndex = fBufferSize;

    fNumBytesToStream = numBytesToStream;
    fLimitNumBytesToStream = fNumBytesToStream > 0;
}

void ByteStreamRingBufferSource::seekToByteRelative(int64_t offset, u_int64_t numBytesToStream) {
    int64_t newIndex = fCurIndex + offset;
    if (newIndex < 0) {
        fCurIndex = 0;
    }
    else {
        fCurIndex = (u_int64_t)offset;
        if (fCurIndex > fBufferSize) fCurIndex = fBufferSize;
    }

    fNumBytesToStream = numBytesToStream;
    fLimitNumBytesToStream = fNumBytesToStream > 0;
}

void ByteStreamRingBufferSource::feedFrame(PBYTE pCurFrame, unsigned frameSize, LONGLONG llSampleDurationhns)
{
    //llLatestSampleDurationus = (long)(llSampleDurationhns * 100 / 1000);
    if (frameSize >= fBufferSize)
    {
        assert(0);
        return;
    }
    if (wptr + frameSize > fBufferSize)
    {
        eor = wptr;
        wptr = 0;
    }
    RtlCopyMemory(fBuffer + wptr, pCurFrame, frameSize);
    wptr += frameSize;
}

void ByteStreamRingBufferSource::doGetNextFrame() {
    if (fCurIndex >= fBufferSize || (fLimitNumBytesToStream && fNumBytesToStream == 0)) {
        handleClosure();
        return;
    }

    // Try to read as many bytes as will fit in the buffer provided (or "fPreferredFrameSize" if less)
    BOOL bResetRptr = false;
    fFrameSize = fMaxSize;
    if (fFrameSize > eor - rptr)
    {
        fFrameSize = eor - rptr;
        bResetRptr = true;
    }

    memmove(fTo, &fBuffer[rptr], fFrameSize);
    if (bResetRptr)
    {
        rptr = 0;
    }
    else
    {
        rptr += fFrameSize;
    }

    if (llLatestSampleDurationus)
    {
        if (fPresentationTime.tv_sec == 0 && fPresentationTime.tv_usec == 0)
        {
            // This is the first frame, so use the current time:
            gettimeofday(&fPresentationTime, NULL);
        }
        else {
            long uSeconds = fPresentationTime.tv_usec + llLatestSampleDurationus;
            fPresentationTime.tv_sec += uSeconds / 1000000;
            fPresentationTime.tv_usec = uSeconds % 1000000;
        }
    }
    else
    {
        gettimeofday(&fPresentationTime, NULL);
    }

    // Inform the downstream object that it has data:
    FramedSource::afterGetting(this);
}
