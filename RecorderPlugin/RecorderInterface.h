#pragma once

#ifndef __RECORDER_INTERFACE_H__
#define __RECORDER_INTERFACE_H__

#include <Windows.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef RECORDERPLUGIN_EXPORTS
#define RECORDER_INTERFACE_API __declspec(dllexport) 
#else
#define RECORDER_INTERFACE_API __declspec(dllimport) 
#endif

    namespace RecorderInterface
    {
        typedef enum _RECORD_STATUS
        {
            RECORD_STATUS_STOPPED,
            RECORD_STATUS_STARTED,
            RECORD_STATUS_PAUSED,
        }RECORD_STATUS;

        RECORDER_INTERFACE_API BOOL StartRecording(BOOL bStreaming, HANDLE hRT, PWCHAR pFileName, UINT32 width, UINT32 height, UINT32 bitRate, UINT32 fps);
        RECORDER_INTERFACE_API VOID PauseRecording();
        RECORDER_INTERFACE_API VOID ResumeRecording();
        RECORDER_INTERFACE_API BOOL StopRecording();
        RECORDER_INTERFACE_API VOID NotifyFrameUpdate();
        RECORDER_INTERFACE_API RECORD_STATUS GetRecordStatus();
        RECORDER_INTERFACE_API VOID SetAudioMute(BOOL bMute);
        RECORDER_INTERFACE_API BOOL GetAudioMuteStatus();
    }

#ifdef __cplusplus
}
#endif

#endif //__RECORDER_INTERFACE_H__
