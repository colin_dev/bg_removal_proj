#include "stdafx.h"
#include "PresentationTimeStamp.h"


PresentationTimeStamp::PresentationTimeStamp() :
    m_hTimer(NULL),
    m_hTimerQueue(NULL),
    m_evReset(NULL),
    m_evActive(NULL),
    m_qpfFreq{0},
    m_qpcStart{0},
    m_qpcPauseStart{0},
    m_qpcPauseDuration{0},
    m_pts(0),
    m_ptsNonPause(0)
{
    ::InitializeCriticalSection(&m_cs);

    m_evReset = ::CreateEvent(NULL, TRUE, FALSE, NULL);
    m_evActive = ::CreateEvent(NULL, TRUE, FALSE, NULL);

    if (m_hTimerQueue = ::CreateTimerQueue())
    {
        BOOL bIsSuccess = ::CreateTimerQueueTimer(
            &m_hTimer, 
            m_hTimerQueue,
            (WAITORTIMERCALLBACK)PseudoClock,
            this, 
            m_clkInterval, 
            m_clkInterval, 
            WT_EXECUTEDEFAULT);
    }

    ::QueryPerformanceFrequency(&m_qpfFreq);
}

PresentationTimeStamp::~PresentationTimeStamp()
{
    if(m_hTimerQueue && m_hTimer) DeleteTimerQueueTimer(m_hTimerQueue, m_hTimer, INVALID_HANDLE_VALUE);
    if(m_hTimerQueue) DeleteTimerQueue(m_hTimerQueue);
    SafeCloseHandle(m_evActive);
    SafeCloseHandle(m_evReset);
    ::DeleteCriticalSection(&m_cs);
}

VOID PresentationTimeStamp::GetPTS(REFERENCE_TIME &pts, REFERENCE_TIME &ptsNonStop)
{
    ::EnterCriticalSection(&m_cs);
    pts = m_pts;
    ptsNonStop = m_ptsNonPause;
    ::LeaveCriticalSection(&m_cs);
}

VOID PresentationTimeStamp::GetLastPTS(REFERENCE_TIME &lastPts)
{
    ::EnterCriticalSection(&m_cs);
    lastPts = m_lastPts;
    ::LeaveCriticalSection(&m_cs);
}

VOID PresentationTimeStamp::SaveLastPTS(REFERENCE_TIME lastPts)
{
    ::EnterCriticalSection(&m_cs);
    m_lastPts = lastPts;
    ::LeaveCriticalSection(&m_cs);
}

VOID PresentationTimeStamp::ReStartPTS()
{
    ::SetEvent(m_evReset);
    ::SetEvent(m_evActive);
}

VOID PresentationTimeStamp::PausePTS()
{
    ::ResetEvent(m_evActive);
    ::QueryPerformanceCounter(&m_qpcPauseStart);
}

VOID PresentationTimeStamp::ResumePTS()
{
    LARGE_INTEGER curMeasure = { 0 };
    ::QueryPerformanceCounter(&curMeasure);
    if (curMeasure.QuadPart > m_qpcPauseStart.QuadPart)
    {
        m_qpcPauseDuration.QuadPart += curMeasure.QuadPart - m_qpcPauseStart.QuadPart;
    }
    ::SetEvent(m_evActive);
}

VOID CALLBACK PresentationTimeStamp::PseudoClock(PVOID pParam, BOOLEAN bTimerOrWaitFired)
{
    PresentationTimeStamp *pThis = (PresentationTimeStamp*)pParam;
    LARGE_INTEGER curMeasure = {0};
    ::QueryPerformanceCounter(&curMeasure);
    ::EnterCriticalSection(&pThis->m_cs);
    if (::WaitForSingleObjectEx(pThis->m_evActive, 0, FALSE) == WAIT_OBJECT_0)
    {
        if (::WaitForSingleObjectEx(pThis->m_evReset, 0, FALSE) == WAIT_OBJECT_0)
        {
            ::QueryPerformanceCounter(&pThis->m_qpcStart);
            pThis->m_qpcPauseStart = { 0 };
            pThis->m_qpcPauseDuration = { 0 };
            ::ResetEvent(pThis->m_evReset);
            pThis->m_pts = 0;
            pThis->m_lastPts = 0;
            pThis->m_ptsNonPause = 0;
        }
        else
        {
            pThis->m_pts = (curMeasure.QuadPart - pThis->m_qpcStart.QuadPart - pThis->m_qpcPauseDuration.QuadPart) * pThis->REFTIMES_PER_SEC / pThis->m_qpfFreq.QuadPart;
        }
    }
    pThis->m_ptsNonPause = (curMeasure.QuadPart - pThis->m_qpcStart.QuadPart) * pThis->REFTIMES_PER_SEC / pThis->m_qpfFreq.QuadPart;
    ::LeaveCriticalSection(&pThis->m_cs);
}