// RecorderTest.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
// Windows
#include <windows.h>
#include <vector>
#include <iostream>

// DX
#include "inc/DxCommon.h"
#include "DirectXTK/inc/DDSTextureLoader.h"
#include "DirectXTK/inc/SpriteBatch.h"
#include "DirectXTK/inc/SpriteFont.h"
// shader
#include "VertexShader.h"
#include "PixelShader.h"

// Recorder Plugin
#include "RecorderInterface.h"

#include "resource.h"

using namespace std;

// Format constants
const UINT32 WINDOW_WIDTH = 1280;
const UINT32 WINDOW_HEIGHT = 960;

const UINT32 VIDEO_WIDTH = 640;
const UINT32 VIDEO_HEIGHT = 480;
const UINT32 VIDEO_FPS = 85;
const UINT32 VIDEO_BIT_RATE = 800000;
const BOOL USE_LIVE_STREAMING = FALSE;
#define LIVE_STREAM_NAME L"live"

typedef enum _RECORD_STATUS
{
    RECORD_STATUS_STOPPED,
    RECORD_STATUS_STARTED,
    RECORD_STATUS_PAUSED,
}RECORD_STATUS;

//--------------------------------------------------------------------------------------
// Global Variables
//--------------------------------------------------------------------------------------
typedef struct _THREAD_DATA
{
    HANDLE hThread;
    DWORD tId;
    HANDLE evExit;
    HANDLE evTimer;
}THREAD_DATA, *PTHREAD_DATA;

HINSTANCE                           g_hInst = NULL;
HWND                                g_hMainWnd = NULL;
HWND                                g_hControlWnd = NULL;
RECORD_STATUS                       g_RecordStatus = RECORD_STATUS_STOPPED;
BOOL                                g_bMute = FALSE;
THREAD_DATA                         g_TData = { 0 };

//--------------------------------------------------------------------------------------
// Forward declarations
//--------------------------------------------------------------------------------------
HRESULT InitWindow(HINSTANCE, int);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK DlgProc(HWND, UINT, WPARAM, LPARAM);
VOID StartRecord(PTHREAD_DATA pTData);
VOID StopRecord(PTHREAD_DATA pTData);
VOID PauseRecord();
VOID ResumeRecord();
DWORD WINAPI RenderThread(_In_ PVOID pParam);
VOID CALLBACK PerodicTimerCB(PVOID pParam, BOOLEAN bTimerOrWaitFired);

//--------------------------------------------------------------------------------------
// Entry point to the program. Initializes everything and goes into a message processing 
// loop. Idle time is used to render the scene.
//--------------------------------------------------------------------------------------
int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    BOOL isSuccess = FALSE;

    if (FAILED(InitWindow(hInstance, nCmdShow)))
        return 0;

    g_TData.evExit = ::CreateEvent(NULL, TRUE, FALSE, NULL);
    g_TData.evTimer = ::CreateEvent(NULL, TRUE, FALSE, NULL);

    HANDLE hTimer = NULL;
    HANDLE hTimerQueue = NULL;
    hTimerQueue = CreateTimerQueue();
    if (hTimerQueue)
    {
        if (CreateTimerQueueTimer(&hTimer, hTimerQueue,
            (WAITORTIMERCALLBACK)PerodicTimerCB, &g_TData.evTimer, 1000 / VIDEO_FPS, 1000 / VIDEO_FPS, WT_EXECUTEDEFAULT))
        {
            isSuccess = TRUE;
        }
    }

    // Main message loop
    MSG msg = { 0 };
    while (WM_QUIT != msg.message)
    {
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        ::Sleep(1000 / VIDEO_FPS);
    }

    if (g_RecordStatus != RECORD_STATUS_STOPPED)
    {
        StopRecord(&g_TData);
    }
    if (isSuccess)
    {
        DeleteTimerQueueTimer(hTimerQueue, hTimer, NULL);
        DeleteTimerQueue(hTimerQueue);
    }
    SafeCloseHandle(g_TData.evExit);
    SafeCloseHandle(g_TData.evTimer);

    return (int)msg.wParam;
}

//--------------------------------------------------------------------------------------
// Register class and create window
//--------------------------------------------------------------------------------------
HRESULT InitWindow(HINSTANCE hInstance, int nCmdShow)
{
    // Register class
    WNDCLASSEX wcex;
    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = WndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon(hInstance, (LPCTSTR)IDI_APPICON);
    wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wcex.lpszMenuName = NULL;
    wcex.lpszClassName = L"RecorderTestWindowClass";
    wcex.hIconSm = LoadIcon(wcex.hInstance, (LPCTSTR)IDI_APPICON);
    if (!RegisterClassEx(&wcex))
        return E_FAIL;

    // Create window
    g_hInst = hInstance;
    RECT rc = { 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT};
    AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);
    g_hMainWnd = CreateWindow(L"RecorderTestWindowClass", L"RecorderTest", WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, NULL, NULL, hInstance,
        NULL);
    if (!g_hMainWnd)
    {
        return E_FAIL;
    }

    ShowWindow(g_hMainWnd, nCmdShow);
    UpdateWindow(g_hMainWnd);

    g_hControlWnd = ::CreateDialog(hInstance, MAKEINTRESOURCE(DLG_CONTROL), NULL, (DLGPROC)DlgProc);
    if (!g_hControlWnd)
    {
        return E_FAIL;
    }

    ShowWindow(g_hControlWnd, nCmdShow);
    UpdateWindow(g_hControlWnd);

    return S_OK;
}

//--------------------------------------------------------------------------------------
// Called every time the application receives a message
//--------------------------------------------------------------------------------------
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HDC hdc;

    switch (message)
    {
    case WM_PAINT:
        hdc = BeginPaint(hWnd, &ps);
        EndPaint(hWnd, &ps);
        break;

    case WM_DESTROY:
        PostQuitMessage(0);
        break;

    case WM_SIZE:
        break;

    case WM_MOVE:
        break;

    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }

    return 0;
}

LRESULT CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);

    switch (message)
    {
    case WM_INITDIALOG:
        EnableWindow(GetDlgItem(hWnd, IDC_BUTTON_START), TRUE);
        EnableWindow(GetDlgItem(hWnd, IDC_BUTTON_PAUSE), FALSE);
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    case WM_COMMAND:
        if (LOWORD(wParam) == IDCANCEL)
        {
            PostQuitMessage(0);
            return (INT_PTR)TRUE;
        }
        else if ((LOWORD(wParam) == IDC_BUTTON_START) || (LOWORD(wParam) == IDC_BUTTON_PAUSE))
        {
            wchar_t *btnStartCap = nullptr;
            wchar_t *btnPauseCap = nullptr;
            // State Transition:
            // RECORD_STATUS_STOPPED:
            //     RECORD_STATUS_STARTED
            // RECORD_STATUS_STARTED:
            //     RECORD_STATUS_PAUSED, RECORD_STATUS_STOPPED
            // RECORD_STATUS_PAUSED:
            //     RECORD_STATUS_STARTED(resume), RECORD_STATUS_STOPPED
            if (g_RecordStatus == RECORD_STATUS_STOPPED)
            {
                if (LOWORD(wParam) == IDC_BUTTON_START)
                {
                    EnableWindow(GetDlgItem(hWnd, IDC_BUTTON_PAUSE), TRUE);
                    StartRecord(&g_TData);
                    g_RecordStatus = RECORD_STATUS_STARTED;
                    btnStartCap = L"Stop";
                    btnPauseCap = L"Pause";
                }
            }
            else if (g_RecordStatus == RECORD_STATUS_STARTED)
            {
                if (LOWORD(wParam) == IDC_BUTTON_START)
                {
                    EnableWindow(GetDlgItem(hWnd, IDC_BUTTON_PAUSE), FALSE);
                    StopRecord(&g_TData);
                    g_RecordStatus = RECORD_STATUS_STOPPED;
                    btnStartCap = L"Start";
                    btnPauseCap = L"Pause";
                }
                else if (LOWORD(wParam) == IDC_BUTTON_PAUSE)
                {
                    EnableWindow(GetDlgItem(hWnd, IDC_BUTTON_PAUSE), TRUE);
                    PauseRecord();
                    g_RecordStatus = RECORD_STATUS_PAUSED;
                    btnStartCap = L"Stop";
                    btnPauseCap = L"Resume";
                }
            }
            else if (g_RecordStatus == RECORD_STATUS_PAUSED)
            {
                if (LOWORD(wParam) == IDC_BUTTON_START)
                {
                    EnableWindow(GetDlgItem(hWnd, IDC_BUTTON_PAUSE), FALSE);
                    StopRecord(&g_TData);
                    g_RecordStatus = RECORD_STATUS_STOPPED;
                    btnStartCap = L"Start";
                    btnPauseCap = L"Pause";
                }
                else if (LOWORD(wParam) == IDC_BUTTON_PAUSE)
                {
                    EnableWindow(GetDlgItem(hWnd, IDC_BUTTON_PAUSE), TRUE);
                    ResumeRecord();
                    g_RecordStatus = RECORD_STATUS_STARTED;
                    btnStartCap = L"Stop";
                    btnPauseCap = L"Pause";
                }
            }

            SendMessage(GetDlgItem(hWnd, IDC_BUTTON_START), WM_SETTEXT, (WPARAM)NULL, (LPARAM)btnStartCap);
            SendMessage(GetDlgItem(hWnd, IDC_BUTTON_PAUSE), WM_SETTEXT, (WPARAM)NULL, (LPARAM)btnPauseCap);
        }
        else if (LOWORD(wParam) == IDC_BUTTON_MUTE)
        {
            g_bMute = !g_bMute;
            wchar_t *btnMuteCap = btnMuteCap = g_bMute ? L"UnMute" : L"Mute";
            SendMessage(GetDlgItem(hWnd, IDC_BUTTON_MUTE), WM_SETTEXT, (WPARAM)NULL, (LPARAM)btnMuteCap);
            RecorderInterface::SetAudioMute(g_bMute);
        }
        break;
    }

    return (INT_PTR)FALSE;
}

DWORD WINAPI RenderThread(_In_ PVOID pParam)
{
    PTHREAD_DATA pTData = (PTHREAD_DATA)pParam;

    RECT rc;
    GetClientRect(g_hMainWnd, &rc);
    UINT width = rc.right - rc.left;
    UINT height = rc.bottom - rc.top;

    D3D11_VIEWPORT vp;
    vp.Width = (FLOAT)width;
    vp.Height = (FLOAT)height;
    vp.MinDepth = 0.0f;
    vp.MaxDepth = 1.0f;
    vp.TopLeftX = 0;
    vp.TopLeftY = 0;

    HRESULT hr = S_OK;
    D3D_DRIVER_TYPE driverType = D3D_DRIVER_TYPE_NULL;
    D3D_FEATURE_LEVEL featureLevel = D3D_FEATURE_LEVEL_11_1;
    ID3D11Device *pDx11Dev = NULL;
    ID3D11DeviceContext *pDx11DevCtx = NULL;
    IDXGISwapChain *pDxgiSwapChain = NULL;
    ID3D11Texture2D *pBackBuffer = NULL;
    ID3D11RenderTargetView *pRTV = NULL;
    ID3D11Texture2D *pDSTex = NULL;
    ID3D11DepthStencilView *pDSV = NULL;
    ID3D11VertexShader *pVS = NULL;
    ID3D11PixelShader *pPS = NULL;
    ID3D11InputLayout *pIL = NULL;
    ID3D11Buffer *pVB = NULL;
    ID3D11Buffer *pIB = NULL;
    ID3D11Buffer *pCBNeverChanges = NULL;
    ID3D11Buffer *pCBChangeOnResize = NULL;
    ID3D11Buffer *pCBChangesEveryFrame = NULL;
    ID3D11Resource *pTex = NULL;
    ID3D11ShaderResourceView *pTexSRV = NULL;
    ID3D11SamplerState *pSS = NULL;
    XMMATRIX mWorld;
    XMMATRIX mView;
    XMMATRIX mProj;
    XMFLOAT4 vMeshColor(0.7f, 0.7f, 0.7f, 1.0f);
    ID3D11Texture2D *pSharedTexRGB = NULL;
    HANDLE hSharedTexRGB = NULL;
    D3D11_TEXTURE2D_DESC texDesc = { 0 };
    std::unique_ptr<DirectX::SpriteBatch> pSB;
    std::unique_ptr<DirectX::SpriteFont> pSF;

    UINT createDeviceFlags = 0;
#ifdef _DEBUG
    createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

    D3D_DRIVER_TYPE driverTypes[] =
    {
        D3D_DRIVER_TYPE_HARDWARE,
        D3D_DRIVER_TYPE_WARP,
        D3D_DRIVER_TYPE_REFERENCE,
    };
    UINT numDriverTypes = ARRAYSIZE(driverTypes);

    D3D_FEATURE_LEVEL featureLevels[] =
    {
        D3D_FEATURE_LEVEL_11_1,
        D3D_FEATURE_LEVEL_11_0,
    };
    UINT numFeatureLevels = ARRAYSIZE(featureLevels);

    DXGI_SWAP_CHAIN_DESC sd;
    ZeroMemory(&sd, sizeof(sd));
    sd.BufferCount = 1;
    sd.BufferDesc.Width = width;
    sd.BufferDesc.Height = height;
    sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;

    sd.BufferDesc.RefreshRate.Numerator = VIDEO_FPS;
    sd.BufferDesc.RefreshRate.Denominator = 1;
    sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    sd.OutputWindow = g_hMainWnd;
    sd.SampleDesc.Count = 1;
    sd.SampleDesc.Quality = 0;
    sd.Windowed = TRUE;
    sd.SwapEffect = DXGI_SWAP_EFFECT_SEQUENTIAL;

    for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
    {
        driverType = driverTypes[driverTypeIndex];
        hr = D3D11CreateDeviceAndSwapChain(NULL, driverType, NULL, createDeviceFlags, featureLevels, numFeatureLevels,
            D3D11_SDK_VERSION, &sd, &pDxgiSwapChain, &pDx11Dev, &featureLevel, &pDx11DevCtx);
        if (SUCCEEDED(hr))
            break;
    }

    // Create a render target view
    if (SUCCEEDED(hr))
    {
        hr = pDxgiSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);
    }

    if (SUCCEEDED(hr))
    {
        hr = pDx11Dev->CreateRenderTargetView(pBackBuffer, NULL, &pRTV);
    }

    // Create depth stencil texture
    D3D11_TEXTURE2D_DESC descDepth;
    ZeroMemory(&descDepth, sizeof(descDepth));
    descDepth.Width = width;
    descDepth.Height = height;
    descDepth.MipLevels = 1;
    descDepth.ArraySize = 1;
    descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    descDepth.SampleDesc.Count = 1;
    descDepth.SampleDesc.Quality = 0;
    descDepth.Usage = D3D11_USAGE_DEFAULT;
    descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
    descDepth.CPUAccessFlags = 0;
    descDepth.MiscFlags = 0;
    if (SUCCEEDED(hr))
    {
        hr = pDx11Dev->CreateTexture2D(&descDepth, NULL, &pDSTex);
    }

    // Create the depth stencil view
    D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
    ZeroMemory(&descDSV, sizeof(descDSV));
    descDSV.Format = descDepth.Format;
    descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    descDSV.Texture2D.MipSlice = 0;
    if (SUCCEEDED(hr))
    {
        hr = pDx11Dev->CreateDepthStencilView(pDSTex, &descDSV, &pDSV);
    }

    // Prepare VS
    if (SUCCEEDED(hr))
    {
        hr = pDx11Dev->CreateVertexShader(g_VS, ARRAYSIZE(g_VS), nullptr, &pVS);
    }

    // Define the input layout
    D3D11_INPUT_ELEMENT_DESC layout[] =
    {
        { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
    };
    UINT numElements = ARRAYSIZE(layout);

    // Create the input layout
    if (SUCCEEDED(hr))
    {
        hr = pDx11Dev->CreateInputLayout(layout, numElements, g_VS, ARRAYSIZE(g_VS), &pIL);
    }

    // Prepare PS
    if (SUCCEEDED(hr))
    {
        hr = pDx11Dev->CreatePixelShader(g_PS, ARRAYSIZE(g_PS), nullptr, &pPS);
    }

    // Create VB
    VERTEX vertices[] =
    {
        { XMFLOAT3(-1.0f, 1.0f, -1.0f), XMFLOAT2(0.0f, 0.0f) },
        { XMFLOAT3(1.0f, 1.0f, -1.0f), XMFLOAT2(1.0f, 0.0f) },
        { XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT2(1.0f, 1.0f) },
        { XMFLOAT3(-1.0f, 1.0f, 1.0f), XMFLOAT2(0.0f, 1.0f) },

        { XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT2(0.0f, 0.0f) },
        { XMFLOAT3(1.0f, -1.0f, -1.0f), XMFLOAT2(1.0f, 0.0f) },
        { XMFLOAT3(1.0f, -1.0f, 1.0f), XMFLOAT2(1.0f, 1.0f) },
        { XMFLOAT3(-1.0f, -1.0f, 1.0f), XMFLOAT2(0.0f, 1.0f) },

        { XMFLOAT3(-1.0f, -1.0f, 1.0f), XMFLOAT2(0.0f, 0.0f) },
        { XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT2(1.0f, 0.0f) },
        { XMFLOAT3(-1.0f, 1.0f, -1.0f), XMFLOAT2(1.0f, 1.0f) },
        { XMFLOAT3(-1.0f, 1.0f, 1.0f), XMFLOAT2(0.0f, 1.0f) },

        { XMFLOAT3(1.0f, -1.0f, 1.0f), XMFLOAT2(0.0f, 0.0f) },
        { XMFLOAT3(1.0f, -1.0f, -1.0f), XMFLOAT2(1.0f, 0.0f) },
        { XMFLOAT3(1.0f, 1.0f, -1.0f), XMFLOAT2(1.0f, 1.0f) },
        { XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT2(0.0f, 1.0f) },

        { XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT2(0.0f, 0.0f) },
        { XMFLOAT3(1.0f, -1.0f, -1.0f), XMFLOAT2(1.0f, 0.0f) },
        { XMFLOAT3(1.0f, 1.0f, -1.0f), XMFLOAT2(1.0f, 1.0f) },
        { XMFLOAT3(-1.0f, 1.0f, -1.0f), XMFLOAT2(0.0f, 1.0f) },

        { XMFLOAT3(-1.0f, -1.0f, 1.0f), XMFLOAT2(0.0f, 0.0f) },
        { XMFLOAT3(1.0f, -1.0f, 1.0f), XMFLOAT2(1.0f, 0.0f) },
        { XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT2(1.0f, 1.0f) },
        { XMFLOAT3(-1.0f, 1.0f, 1.0f), XMFLOAT2(0.0f, 1.0f) },
    };

    D3D11_BUFFER_DESC bd;
    ZeroMemory(&bd, sizeof(bd));
    bd.Usage = D3D11_USAGE_DEFAULT;
    bd.ByteWidth = sizeof(VERTEX) * 24;
    bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    bd.CPUAccessFlags = 0;
    D3D11_SUBRESOURCE_DATA InitData;
    ZeroMemory(&InitData, sizeof(InitData));
    InitData.pSysMem = vertices;
    if (SUCCEEDED(hr))
    {
        hr = pDx11Dev->CreateBuffer(&bd, &InitData, &pVB);
    }

    // Set vertex buffer
    UINT stride = sizeof(VERTEX);
    UINT offset = 0;

    // Create IB
    WORD indices[] =
    {
        3,1,0,
        2,1,3,

        6,4,5,
        7,4,6,

        11,9,8,
        10,9,11,

        14,12,13,
        15,12,14,

        19,17,16,
        18,17,19,

        22,20,21,
        23,20,22
    };

    bd.Usage = D3D11_USAGE_DEFAULT;
    bd.ByteWidth = sizeof(WORD) * 36;
    bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
    bd.CPUAccessFlags = 0;
    InitData.pSysMem = indices;
    if (SUCCEEDED(hr))
    {
        hr = pDx11Dev->CreateBuffer(&bd, &InitData, &pIB);
    }

    // Create the constant buffers
    bd.Usage = D3D11_USAGE_DEFAULT;
    bd.ByteWidth = sizeof(CBNeverChanges);
    bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    bd.CPUAccessFlags = 0;
    if (SUCCEEDED(hr))
    {
        hr = pDx11Dev->CreateBuffer(&bd, NULL, &pCBNeverChanges);
    }

    bd.ByteWidth = sizeof(CBChangeOnResize);
    if (SUCCEEDED(hr))
    {
        hr = pDx11Dev->CreateBuffer(&bd, NULL, &pCBChangeOnResize);
    }

    bd.ByteWidth = sizeof(CBChangesEveryFrame);
    if (SUCCEEDED(hr))
    {
        hr = pDx11Dev->CreateBuffer(&bd, NULL, &pCBChangesEveryFrame);
    }

    // Load texture from resource
    HRSRC texRes = ::FindResource(NULL, MAKEINTRESOURCE(IDR_TEXTURE1), MAKEINTRESOURCE(RC_TEXTURE));
    unsigned int texResSize = ::SizeofResource(g_hInst, texRes);
    HGLOBAL texResData = ::LoadResource(NULL, texRes);
    void* pTexResBinary = ::LockResource(texResData);
    if (SUCCEEDED(hr))
    {
        hr = CreateDDSTextureFromMemory(pDx11Dev, (const uint8_t*)pTexResBinary, (size_t)texResSize, &pTex, &pTexSRV);
    }

    pBackBuffer->GetDesc(&texDesc);
    texDesc.Usage = D3D11_USAGE_DEFAULT;
    texDesc.BindFlags = D3D11_BIND_RENDER_TARGET;
    texDesc.CPUAccessFlags = 0;
    texDesc.MiscFlags = D3D11_RESOURCE_MISC_SHARED;
    if (SUCCEEDED(hr))
    {
        hr = pDx11Dev->CreateTexture2D(&texDesc, NULL, &pSharedTexRGB);
    }

    IDXGIResource *pDXGIResource = nullptr;
    if (SUCCEEDED(hr))
    {
        hr = pSharedTexRGB->QueryInterface(__uuidof(IDXGIResource), reinterpret_cast<void**>(&pDXGIResource));
    }
    if (SUCCEEDED(hr))
    {
        pDXGIResource->GetSharedHandle(&hSharedTexRGB);
    }
    SafeRelease(pDXGIResource);

    // Create the sample state
    D3D11_SAMPLER_DESC sampDesc;
    ZeroMemory(&sampDesc, sizeof(sampDesc));
    sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
    sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
    sampDesc.MinLOD = 0;
    sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
    if (SUCCEEDED(hr))
    {
        hr = pDx11Dev->CreateSamplerState(&sampDesc, &pSS);
    }

    // Initialize the world matrices
    mWorld = XMMatrixIdentity();

    // Initialize the view matrix
    XMVECTOR Eye = XMVectorSet(0.0f, 3.0f, -6.0f, 0.0f);
    XMVECTOR At = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
    XMVECTOR Up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
    mView = XMMatrixLookAtLH(Eye, At, Up);

    CBNeverChanges cbNeverChanges;
    cbNeverChanges.mView = XMMatrixTranspose(mView);
    pDx11DevCtx->UpdateSubresource(pCBNeverChanges, 0, NULL, &cbNeverChanges, 0, 0);

    // Initialize the projection matrix
    mProj = XMMatrixPerspectiveFovLH(XM_PIDIV4, width / (FLOAT)height, 0.01f, 100.0f);

    CBChangeOnResize cbChangesOnResize;
    cbChangesOnResize.mProjection = XMMatrixTranspose(mProj);
    pDx11DevCtx->UpdateSubresource(pCBChangeOnResize, 0, NULL, &cbChangesOnResize, 0, 0);

    // Load texture from resource
    HRSRC fontRes = ::FindResource(NULL, MAKEINTRESOURCE(IDR_FONT1), MAKEINTRESOURCE(RC_FONT));
    unsigned int fontResSize = ::SizeofResource(g_hInst, fontRes);
    HGLOBAL fontResData = ::LoadResource(NULL, fontRes);
    void* pFontResBinary = ::LockResource(fontResData);
    if (SUCCEEDED(hr))
    {
        pSF = std::make_unique<SpriteFont>(pDx11Dev, (uint8_t*)pFontResBinary, fontResSize);
    }
    pSB = std::make_unique<SpriteBatch>(pDx11DevCtx);

    // Prepare recording
    if (SUCCEEDED(hr))
    {
        WCHAR fName[MAX_PATH * 2];
        ZeroMemory(fName, MAX_PATH * 2 * sizeof(WCHAR));

        if (USE_LIVE_STREAMING)
        {
            swprintf_s(fName, MAX_PATH * 2, L"%s", LIVE_STREAM_NAME);
        }
        else
        {
            SYSTEMTIME curTime = { 0 };
            GetLocalTime(&curTime);
            WCHAR path[MAX_PATH * 2];
            ZeroMemory(path, MAX_PATH * 2 * sizeof(WCHAR));
            GetCurrentDirectory(MAX_PATH * 2, path);

            swprintf_s(fName, MAX_PATH * 2, L"%s\\%04d%02d%02d_%02d%02d%02d.mp4",
                path, curTime.wYear, curTime.wMonth, curTime.wDay, curTime.wHour, curTime.wMinute, curTime.wSecond);
        }

        if (RecorderInterface::RECORD_STATUS_STOPPED == RecorderInterface::GetRecordStatus())
        {
            BOOL isSuccess = RecorderInterface::StartRecording(
                USE_LIVE_STREAMING, hSharedTexRGB, fName, VIDEO_WIDTH, VIDEO_HEIGHT, VIDEO_BIT_RATE, VIDEO_FPS);
            hr = isSuccess ? S_OK : E_FAIL;
            if (SUCCEEDED(hr))
            {
                g_bMute = RecorderInterface::GetAudioMuteStatus();
            }
        }
    }

    UINT32 frameCount = 0;
    WCHAR strBuf[MAX_PATH] = { 0 };
    // Render loop
    while ((SUCCEEDED(hr)) && (::WaitForSingleObjectEx(pTData->evExit, 0, FALSE) == WAIT_TIMEOUT))
    {
        if (::WaitForSingleObjectEx(pTData->evTimer, 0, FALSE) == WAIT_OBJECT_0)
        {
            if (::WaitForSingleObjectEx(pTData->evExit, 0, FALSE) == WAIT_OBJECT_0)
            {
                break;
            }
            ::ResetEvent(pTData->evTimer);
            static DWORD dwTimeStart = 0;
            DWORD dwTimeCur = GetTickCount();
            if (dwTimeStart == 0)
                dwTimeStart = dwTimeCur;
            float t = (dwTimeCur - dwTimeStart) / 300.0f;

            // Rotate cube around the origin
            mWorld = XMMatrixRotationY(t);

            // Modify the color
            //vMeshColor.x = ( sinf( t * 1.0f ) + 1.0f ) * 0.5f;
            //vMeshColor.y = ( cosf( t * 3.0f ) + 1.0f ) * 0.5f;
            //vMeshColor.z = ( sinf( t * 5.0f ) + 1.0f ) * 0.5f;
            //pDx11DevCtx->OMSetRenderTargets(1, &pRTV, NULL);

            //
            // Clear the back buffer
            //
            float ClearColor[4] = { 0.0f, 0.125f, 0.3f, 1.0f }; // red, green, blue, alpha
            pDx11DevCtx->ClearRenderTargetView(pRTV, ClearColor);

            //
            // Clear the depth buffer to 1.0 (max depth)
            //
            pDx11DevCtx->ClearDepthStencilView(pDSV, D3D11_CLEAR_DEPTH, 1.0f, 0);

            //
            // Update variables that change once per frame
            //
            CBChangesEveryFrame cb;
            cb.mWorld = XMMatrixTranspose(mWorld);
            cb.vMeshColor = vMeshColor;
            pDx11DevCtx->UpdateSubresource(pCBChangesEveryFrame, 0, NULL, &cb, 0, 0);

            // Render
            pDx11DevCtx->OMSetRenderTargets(1, &pRTV, NULL);
            pDx11DevCtx->RSSetViewports(1, &vp);
            pDx11DevCtx->VSSetShader(pVS, NULL, 0);
            pDx11DevCtx->PSSetShader(pPS, NULL, 0);
            pDx11DevCtx->IASetInputLayout(pIL);
            pDx11DevCtx->IASetVertexBuffers(0, 1, &pVB, &stride, &offset);
            pDx11DevCtx->IASetIndexBuffer(pIB, DXGI_FORMAT_R16_UINT, 0);
            pDx11DevCtx->VSSetConstantBuffers(0, 1, &pCBNeverChanges);
            pDx11DevCtx->VSSetConstantBuffers(1, 1, &pCBChangeOnResize);
            pDx11DevCtx->VSSetConstantBuffers(2, 1, &pCBChangesEveryFrame);
            pDx11DevCtx->PSSetConstantBuffers(2, 1, &pCBChangesEveryFrame);
            pDx11DevCtx->PSSetShaderResources(0, 1, &pTexSRV);
            pDx11DevCtx->PSSetSamplers(0, 1, &pSS);
            pDx11DevCtx->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
            pDx11DevCtx->PSSetShaderResources(0, 1, &pTexSRV);
            pDx11DevCtx->DrawIndexed(36, 0, 0);
            swprintf_s(strBuf, MAX_PATH, L"%d", frameCount++);

            pSB->Begin();
            pSF->DrawString(pSB.get(), strBuf, XMFLOAT2((float)width / 2, 10), Colors::Yellow);
            pSB->End();

            pDx11DevCtx->CopyResource(pSharedTexRGB, pBackBuffer);

            RecorderInterface::NotifyFrameUpdate();

            //
            // Present our back buffer to our front buffer
            //
            pDxgiSwapChain->Present(1, 0);
        }
    }

    // Cleanup
    if (pDx11DevCtx) pDx11DevCtx->ClearState();
    SafeRelease(pSharedTexRGB);
    SafeRelease(pSS);
    SafeRelease(pTexSRV);
    SafeRelease(pTex);
    SafeRelease(pCBNeverChanges);
    SafeRelease(pCBChangeOnResize);
    SafeRelease(pCBChangesEveryFrame);
    SafeRelease(pIB);
    SafeRelease(pVB);
    SafeRelease(pIL);
    SafeRelease(pPS);
    SafeRelease(pVS);
    SafeRelease(pDSTex);
    SafeRelease(pDSV);
    SafeRelease(pRTV);
    SafeRelease(pBackBuffer);
    SafeRelease(pDxgiSwapChain);
    SafeRelease(pDx11DevCtx);
    SafeRelease(pDx11Dev);
    return 0;
}

VOID StartRecord(PTHREAD_DATA pTData)
{
    if (pTData)
    {
        ::ResetEvent(pTData->evExit);
        ::ResetEvent(pTData->evTimer);
        pTData->hThread = ::CreateThread(nullptr, 0, RenderThread, pTData, 0, &(pTData->tId));
    }
}

VOID StopRecord(PTHREAD_DATA pTData)
{
    // Stop Recording before stop rendering
    RecorderInterface::StopRecording();
    if (pTData)
    {
        ::SetEvent(pTData->evExit);
        ::WaitForSingleObjectEx(pTData->hThread, 3000, FALSE);
    }
}

VOID PauseRecord()
{
    if ((RecorderInterface::RECORD_STATUS_STARTED == RecorderInterface::GetRecordStatus()))
    {
        RecorderInterface::PauseRecording();
    }
}

VOID ResumeRecord()
{
    if ((RecorderInterface::RECORD_STATUS_PAUSED == RecorderInterface::GetRecordStatus()))
    {
        RecorderInterface::ResumeRecording();
    }
}

VOID CALLBACK PerodicTimerCB(PVOID pParam, BOOLEAN bTimerOrWaitFired)
{
    HANDLE ev = *(PHANDLE)pParam;
    ::SetEvent(ev);
}
