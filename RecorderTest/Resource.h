//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by RecorderTest.rc
//
#define IDI_APPICON                     101
#define DLG_CONTROL                     102
#define IDR_TEXTURE1                    130
#define IDR_FONT1                       140
#define IDC_BUTTON_START                1001
#define IDC_BUTTON_PAUSE                1002
#define IDC_BUTTON_MUTE                 1003
#define RC_TEXTURE                      1300
#define RC_FONT                         1400
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1004
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
